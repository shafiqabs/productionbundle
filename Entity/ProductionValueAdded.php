<?php

namespace Terminalbd\ProductionBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * ProductionElement
 *
 * @ORM\Table(name ="pro_value_added")
 * @ORM\Entity(repositoryClass="Terminalbd\ProductionBundle\Repository\ProductionValueAddedRepository")
 */
class ProductionValueAdded
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionItem", inversedBy="productionValueAddeds" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $productionItem;


     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionItemAmendment", inversedBy="productionValueAddeds" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $productionItemAmendment;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting")
     **/
    private  $valueAdded;


    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable = true)
     */
    private $amount = 0;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProductionItem()
    {
        return $this->productionItem;
    }

    /**
     * @param mixed $productionItem
     */
    public function setProductionItem($productionItem)
    {
        $this->productionItem = $productionItem;
    }

    /**
     * @return mixed
     */
    public function getValueAdded()
    {
        return $this->valueAdded;
    }

    /**
     * @param mixed $valueAdded
     */
    public function setValueAdded($valueAdded)
    {
        $this->valueAdded = $valueAdded;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return ProductionItemAmendment
     */
    public function getProductionItemAmendment()
    {
        return $this->productionItemAmendment;
    }

    /**
     * @param ProductionItemAmendment $productionItemAmendment
     */
    public function setProductionItemAmendment($productionItemAmendment)
    {
        $this->productionItemAmendment = $productionItemAmendment;
    }


}

