<?php

namespace Terminalbd\ProcurementBundle\Entity;
use App\Entity\Application\Procurement;
use App\Entity\Domain\ModuleProcessItem;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository")
 * @ORM\Table(name="procu_process")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProcurementProcess
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     */
    private $config;

    /**
     * @var ModuleProcessItem
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcessItem", inversedBy="requisition")
     */
    private $moduleProcessItem;

     /**
     * @var Requisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Requisition", inversedBy="procurementProcess")
     */
    private $requisition;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $process;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isRejected;



    /**
     * @var string
     *
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }



    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(Procurement $config)
    {
        $this->config = $config;
    }

    /**
     * @return Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param Requisition $requisition
     */
    public function setRequisition(Requisition $requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return bool
     */
    public function isRejected(): bool
    {
        return $this->isRejected;
    }

    /**
     * @param bool $isRejected
     */
    public function setIsRejected(bool $isRejected): void
    {
        $this->isRejected = $isRejected;
    }

    /**
     * @return ModuleProcessItem
     */
    public function getModuleProcessItem(): ModuleProcessItem
    {
        return $this->moduleProcessItem;
    }

    /**
     * @param ModuleProcessItem $moduleProcessItem
     */
    public function setModuleProcessItem(ModuleProcessItem $moduleProcessItem): void
    {
        $this->moduleProcessItem = $moduleProcessItem;
    }









}
