<?php

namespace Terminalbd\ProductionBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * ProductionWorkOrderItem
 *
 * @ORM\Table(name ="pro_work_order_item")
 * @ORM\Entity(repositoryClass="Terminalbd\ProductionBundle\Repository\ProductionWorkOrderItemRepository")
 */
class ProductionWorkOrderItem
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionItem", inversedBy="productionWorkOrderItems" )
     **/
    private  $productionItem;



    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionBatchItem", mappedBy="workorderItem" )
     **/
    private  $productionWorkOrderItems;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionWorkOrder", inversedBy="productionWorkOrderItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $productionWorkOrder;


    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable = true)
     */
    private $amount;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $issueQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $receiveQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $damageQuantity;


    /**
     * @var float
     *
     * @ORM\Column(name="subTotal", type="float", nullable = true)
     */
    private $subTotal;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProductionItem()
    {
        return $this->productionItem;
    }

    /**
     * @param mixed $productionItem
     */
    public function setProductionItem($productionItem)
    {
        $this->productionItem = $productionItem;
    }


    /**
     * @return float
     */
    public function getAmount(): ? float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }


    /**
     * @return float
     */
    public function getReceiveQuantity(): ? float
    {
        return $this->receiveQuantity;
    }

    /**
     * @param float $receiveQuantity
     */
    public function setReceiveQuantity(float $receiveQuantity)
    {
        $this->receiveQuantity = $receiveQuantity;
    }

    /**
     * @return float
     */
    public function getDamageQuantity(): ? float
    {
        return $this->damageQuantity;
    }

    /**
     * @param float $damageQuantity
     */
    public function setDamageQuantity(float $damageQuantity)
    {
        $this->damageQuantity = $damageQuantity;
    }

    /**
     * @return float
     */
    public function getSubTotal(): ? float
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal(float $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getIssueQuantity(): ? float
    {
        return $this->issueQuantity;
    }

    /**
     * @param float $issueQuantity
     */
    public function setIssueQuantity(float $issueQuantity)
    {
        $this->issueQuantity = $issueQuantity;
    }

    /**
     * @return ProductionWorkOrder
     */
    public function getProductionWorkOrder()
    {
        return $this->productionWorkOrder;
    }

    /**
     * @param ProductionWorkOrder $productionWorkOrder
     */
    public function setProductionWorkOrder($productionWorkOrder)
    {
        $this->productionWorkOrder = $productionWorkOrder;
    }

}

