<?php

namespace Terminalbd\ProductionBundle\Entity;

use App\Entity\Application\Production;
use App\Entity\Core\Vendor;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
/**
 * Damage
 *
 * @ORM\Table("pro_batch")
 * @ORM\Entity(repositoryClass="Terminalbd\ProductionBundle\Repository\ProductionBatchRepository")
 * @Gedmo\Uploadable(filenameGenerator="SHA1", allowOverwrite=true, appendNumber=true)
 */
class ProductionBatch
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Production")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $config;


    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\ProductionBundle\Entity\InvoiceKeyValue", mappedBy="batch")
     **/
    private  $invoiceKeyValues;

     /**
     * @ORM\OneToMany(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionBatchItem", mappedBy="batch")
     **/
     private  $batchItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Vendor")
     **/
     private  $vendor;


    /**
     * @var integer
     *
     * @ORM\Column(type="integer" , nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , nullable=true, length=20)
     */
    private $mode = 'inhouse';


    /**
     * @var string
     *
     * @ORM\Column(type="string" , nullable=true)
     */
    private $invoice;


     /**
     * @var string
     *
     * @ORM\Column(type="string" , nullable=true)
     */
     private $remark;


    /**
     * @var string
     *
     * @ORM\Column(type="string" , nullable=true)
     */
     private $requsitionNo;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $checkedBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approvedBy;


    /**
     * @var string
     *
     * @ORM\Column(name="process", type="string", length=50, nullable = true)
     */
    private $process="created";

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable = true)
     */
    private $status = false;


    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    private $address;


    /**
     * @var string
     *
     * @ORM\Column(name="issuePerson", type="string",nullable=true)
     */
    private $issuePerson;

    /**
     * @var string
     *
     * @ORM\Column(name="issueDesignation", type="string", nullable=true)
     */
    private $issueDesignation;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $issueTime;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="issueDate", type="datetime")
     */
    private $issueDate;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="receiveDate", type="datetime")
     */
    private $receiveDate;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated", type="datetime", nullable = true)
     */
    private $updated;

    /**
     * @ORM\Column(name="path", type="string", nullable=true)
     * @Gedmo\UploadableFilePath
     */
    protected $path;

    /**
     * @Assert\File(maxSize="8388608")
     */
    protected $file;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return Production
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Production $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getInvoice(): ? string
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice(string $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return string
     */
    public function getRemark(): ? string
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark(string $remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return string
     */
    public function getRequsitionNo(): ? string
    {
        return $this->requsitionNo;
    }

    /**
     * @param string $requsitionNo
     */
    public function setRequsitionNo(string $requsitionNo)
    {
        $this->requsitionNo = $requsitionNo;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getCheckedBy()
    {
        return $this->checkedBy;
    }

    /**
     * @param User $checkedBy
     */
    public function setCheckedBy($checkedBy)
    {
        $this->checkedBy = $checkedBy;
    }

    /**
     * @return User
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param User $approvedBy
     */
    public function setApprovedBy($approvedBy)
    {
        $this->approvedBy = $approvedBy;
    }

    /**
     * @return string
     */
    public function getProcess(): ? string
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return \DateTime
     */
    public function getIssueDate(): ? \DateTime
    {
        return $this->issueDate;
    }

    /**
     * @param \DateTime $issueDate
     */
    public function setIssueDate(\DateTime $issueDate)
    {
        $this->issueDate = $issueDate;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * Sets file.
     *
     * @param User $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return User
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir(). $this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    protected function getUploadRootDir()
    {
        return WEB_ROOT .'/uploads/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'work-order/';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );
        // set the path property to the filename where you've saved the file
        $this->path = $filename;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }


    /**
     * @return \DateTime
     */
    public function getReceiveDate(): \DateTime
    {
        return $this->receiveDate;
    }

    /**
     * @param \DateTime $receiveDate
     */
    public function setReceiveDate(\DateTime $receiveDate)
    {
        $this->receiveDate = $receiveDate;
    }

    /**
     * @return int
     */
    public function getCode(): ? int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return InvoiceKeyValue
     */
    public function getInvoiceKeyValues()
    {
        return $this->invoiceKeyValues;
    }

    /**
     * @return string
     */
    public function getMode(): string
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return mixed
     */
    public function getBatchItems()
    {
        return $this->batchItems;
    }

    /**
     * @param mixed $batchItems
     */
    public function setBatchItems($batchItems)
    {
        $this->batchItems = $batchItems;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return Vendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param Vendor $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return string
     */
    public function getIssuePerson()
    {
        return $this->issuePerson;
    }

    /**
     * @param string $issuePerson
     */
    public function setIssuePerson(string $issuePerson)
    {
        $this->issuePerson = $issuePerson;
    }

    /**
     * @return string
     */
    public function getIssueDesignation()
    {
        return $this->issueDesignation;
    }

    /**
     * @param string $issueDesignation
     */
    public function setIssueDesignation(string $issueDesignation)
    {
        $this->issueDesignation = $issueDesignation;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getIssueTime()
    {
        return $this->issueTime;
    }

    /**
     * @param string $issueTime
     */
    public function setIssueTime($issueTime)
    {
        $this->issueTime = $issueTime;
    }



}

