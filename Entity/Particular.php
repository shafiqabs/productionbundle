<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use App\Entity\Admin\Location;
use Terminalbd\ProcurementBundleBundle\Repository\ParticularRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\ParticularRepository")
 * @ORM\Table(name="procu_particular")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Particular
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     */
    private $config;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $parent;


    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular", mappedBy="parent")
     */
    private $children;


    /**
     * @var ParticularType
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\ParticularType")
     */
    private $particularType;

       /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $code;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $nameBn;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @Doctrine\ORM\Mapping\Column(length=255,unique=false, nullable=true)
     */
    private $slug;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getNameBn()
    {
        return $this->nameBn;
    }

    /**
     * @param string $nameBn
     */
    public function setNameBn($nameBn)
    {
        $this->nameBn = $nameBn;
    }

    public function getNameBanglaEnglish(){

        return $this->nameBn.' - '.$this->name;
    }

    /**
     * @return ParticularType
     */
    public function getParticularType()
    {
        return $this->particularType;
    }


    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(GenericMaster $config)
    {
        $this->config = $config;
    }


    /**
     * @return Particular
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Particular $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }




}
