<?php

namespace Terminalbd\ProductionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\ProductionBundle\Entity\ProductionWorkOrder;

/**
 * InvoiceKeyValue
 *
 * @ORM\Table(name="pro_key_value")
 * @ORM\Entity(repositoryClass="Terminalbd\ProductionBundle\Repository\InvoiceKeyValueRepository")
 */
class InvoiceKeyValue
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionWorkOrder", inversedBy="invoiceKeyValues" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $workorder;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionBatch", inversedBy="invoiceKeyValues" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $batch;


    /**
     * @var string
     *
     * @ORM\Column(name="metaKey", type="string", length=255, nullable = true)
     */
    private $metaKey;

    /**
     * @var string
     *
     * @ORM\Column(name="metaValue", type="string", length=255 , nullable = true)
     */
    private $metaValue;

    /**
     * @var Integer
     *
     * @ORM\Column(name="sorting", type="smallint", length=2, nullable = true)
     */
    private $sorting;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getMetaKey()
    {
        return $this->metaKey;
    }

    /**
     * @param string $metaKey
     */
    public function setMetaKey($metaKey)
    {
        $this->metaKey = $metaKey;
    }

    /**
     * @return string
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }

    /**
     * @param string $metaValue
     */
    public function setMetaValue($metaValue)
    {
        $this->metaValue = $metaValue;
    }


    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * @return ProductionWorkOrder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @param ProductionWorkOrder $workorder
     */
    public function setWorkorder($workorder)
    {
        $this->workorder = $workorder;
    }

    /**
     * @return ProductionBatch
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * @param ProductionBatch $batch
     */
    public function setBatch($batch)
    {
        $this->batch = $batch;
    }


}

