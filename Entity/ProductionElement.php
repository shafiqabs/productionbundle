<?php

namespace Terminalbd\ProductionBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Terminalbd\InventoryBundle\Entity\Item;

/**
 * ProductionElement
 *
 * @ORM\Table(name ="pro_element")
 * @ORM\Entity(repositoryClass="Terminalbd\ProductionBundle\Repository\ProductionElementRepository")
 */
class ProductionElement
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionItem", inversedBy="elements" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $productionItem;

     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionItemAmendment", inversedBy="elements" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $productionItemAmendment;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Item", inversedBy="productionElements" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $material;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $materialQuantity;


    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float")
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $uom;


    /**
     * @var float
     *
     * @ORM\Column(name="purchasePrice", type="float", nullable = true)
     */
    private $purchasePrice;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $price;


    /**
     * @var float
     *
     * @ORM\Column(name="subTotal", type="float", nullable = true)
     */
    private $subTotal;

    /**
     * @var float
     *
     * @ORM\Column(name="wastageQuantity", type="float", nullable = true)
     */
    private $wastageQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $totalQuantity;


    /**
     * @var float
     *
     * @ORM\Column(name="wastagePercent", type="float", nullable = true)
     */
    private $wastagePercent;


    /**
     * @var float
     *
     * @ORM\Column(name="wastageAmount", type="float", nullable = true)
     */
    private $wastageAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="wastageSubTotal", type="float", nullable = true)
     */
    private $wastageSubTotal;



    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @param integer $quantity
     */

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set purchasePrice
     * @param float $purchasePrice
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;
    }

    /**
     * Get purchasePrice
     *
     * @return float
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }


    /**
     * @return mixed
     */
    public function getProductionItem()
    {
        return $this->productionItem;
    }

    /**
     * @param mixed $productionItem
     */
    public function setProductionItem($productionItem)
    {
        $this->productionItem = $productionItem;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal(float $subTotal)
    {
        $this->subTotal = $subTotal;
    }


    /**
     * @return Item
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * @param Item $material
     */
    public function setMaterial($material)
    {
        $this->material = $material;
    }

    /**
     * @return float
     */
    public function getWastageQuantity()
    {
        return $this->wastageQuantity;
    }

    /**
     * @param float $wastageQuantity
     */
    public function setWastageQuantity($wastageQuantity)
    {
        $this->wastageQuantity = $wastageQuantity;
    }

    /**
     * @return float
     */
    public function getWastageAmount()
    {
        return $this->wastageAmount;
    }

    /**
     * @param float $wastageAmount
     */
    public function setWastageAmount(float $wastageAmount)
    {
        $this->wastageAmount = $wastageAmount;
    }

    /**
     * @return float
     */
    public function getWastageSubTotal()
    {
        return $this->wastageSubTotal;
    }

    /**
     * @param float $wastageSubTotal
     */
    public function setWastageSubTotal($wastageSubTotal)
    {
        $this->wastageSubTotal = $wastageSubTotal;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getUom(): ? string
    {
        return $this->uom;
    }

    /**
     * @param string $uom
     */
    public function setUom(string $uom)
    {
        $this->uom = $uom;
    }

    /**
     * @return float
     */
    public function getWastagePercent()
    {
        return $this->wastagePercent;
    }

    /**
     * @param float $wastagePercent
     */
    public function setWastagePercent($wastagePercent)
    {
        $this->wastagePercent = $wastagePercent;
    }

    /**
     * @return ProductionItemAmendment
     */
    public function getProductionItemAmendment()
    {
        return $this->productionItemAmendment;
    }

    /**
     * @param ProductionItemAmendment $productionItemAmendment
     */
    public function setProductionItemAmendment($productionItemAmendment)
    {
        $this->productionItemAmendment = $productionItemAmendment;
    }

    /**
     * @return float
     */
    public function getTotalQuantity(): float
    {
        return $this->totalQuantity;
    }

    /**
     * @return float
     */
    public function getMaterialQuantity(): float
    {
        return $this->materialQuantity;
    }

    /**
     * @param float $materialQuantity
     */
    public function setMaterialQuantity(float $materialQuantity)
    {
        $this->materialQuantity = $materialQuantity;
    }

    /**
     * @param float $totalQuantity
     */
    public function setTotalQuantity(float $totalQuantity)
    {
        $this->totalQuantity = $totalQuantity;
    }




}

