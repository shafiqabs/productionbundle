<?php

namespace Terminalbd\ProductionBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Terminalbd\InventoryBundle\Entity\Item;

/**
 * ProductionWorkOrderItem
 *
 * @ORM\Table(name ="pro_batch_item")
 * @ORM\Entity(repositoryClass="Terminalbd\ProductionBundle\Repository\ProductionBatchItemRepository")
 */
class ProductionBatchItem
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionItem", inversedBy="productionBatchItems" )
     **/
    private  $productionItem;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Item")
     **/
    private  $item;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionWorkOrderItem", inversedBy="productionWorkOrderItems" )
     **/
    private  $workorderItem;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionReceiveBatchItem", mappedBy="batchItem" )
     **/
    private  $receiveItems;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\SalesItem", mappedBy="productionBatchItem"))
     **/
    private  $salesItems;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionExpense", mappedBy="productionBatchItem" )
     **/
    private  $productionExpenses;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionBatch", inversedBy="batchItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $batch;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $issueQuantity;


     /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $receiveQuantity;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $damageQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $returnQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $remainingQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $salesQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $salesReturnQuantity;

     /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $salesDamageQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $stockQuantity;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable = true)
     */
    private $status = "valid";



    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ProductionItem
     */
    public function getProductionItem()
    {
        return $this->productionItem;
    }

    /**
     * @param ProductionItem $productionItem
     */
    public function setProductionItem($productionItem)
    {
        $this->productionItem = $productionItem;
    }


    /**
     * @return float
     */
    public function getDamageQuantity(): ? float
    {
        return $this->damageQuantity;
    }

    /**
     * @param float $damageQuantity
     */
    public function setDamageQuantity(float $damageQuantity)
    {
        $this->damageQuantity = $damageQuantity;
    }



    /**
     * @return float
     */
    public function getIssueQuantity(): ? float
    {
        return $this->issueQuantity;
    }

    /**
     * @param float $issueQuantity
     */
    public function setIssueQuantity(float $issueQuantity)
    {
        $this->issueQuantity = $issueQuantity;
    }

    /**
     * @return ProductionWorkOrderItem
     */
    public function getWorkorderItem()
    {
        return $this->workorderItem;
    }

    /**
     * @param ProductionWorkOrderItem $workorderItem
     */
    public function setWorkorderItem($workorderItem)
    {
        $this->workorderItem = $workorderItem;
    }

    /**
     * @return ProductionBatch
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * @param ProductionBatch $batch
     */
    public function setBatch($batch)
    {
        $this->batch = $batch;
    }

    /**
     * @return float
     */
    public function getReceiveQuantity(): ? float
    {
        return $this->receiveQuantity;
    }

    /**
     * @param float $receiveQuantity
     */
    public function setReceiveQuantity(float $receiveQuantity)
    {
        $this->receiveQuantity = $receiveQuantity;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return ProductionReceiveBatchItem
     */
    public function getReceiveItems()
    {
        return $this->receiveItems;
    }

    /**
     * @return float
     */
    public function getReturnQuantity()
    {
        return $this->returnQuantity;
    }

    /**
     * @param float $returnQuantity
     */
    public function setReturnQuantity(float $returnQuantity)
    {
        $this->returnQuantity = $returnQuantity;
    }

    /**
     * @return float
     */
    public function getRemainingQuantity()
    {
        return $this->remainingQuantity;
    }

    /**
     * @param float $remainingQuantity
     */
    public function setRemainingQuantity( $remainingQuantity)
    {
        $this->remainingQuantity = $remainingQuantity;
    }

    /**
     * @return ProductionExpense
     */
    public function getProductionExpenses()
    {
        return $this->productionExpenses;
    }

    /**
     * @return float
     */
    public function getSalesQuantity()
    {
        return $this->salesQuantity;
    }

    /**
     * @param float $salesQuantity
     */
    public function setSalesQuantity( $salesQuantity)
    {
        $this->salesQuantity = $salesQuantity;
    }

    /**
     * @return float
     */
    public function getStockQuantity()
    {
        return $this->stockQuantity;
    }

    /**
     * @param float $stockQuantity
     */
    public function setStockQuantity( $stockQuantity)
    {
        $this->stockQuantity = $stockQuantity;
    }

    /**
     * @return float
     */
    public function getSalesReturnQuantity()
    {
        return $this->salesReturnQuantity;
    }

    /**
     * @param float $salesReturnQuantity
     */
    public function setSalesReturnQuantity( $salesReturnQuantity)
    {
        $this->salesReturnQuantity = $salesReturnQuantity;
    }

    /**
     * @return float
     */
    public function getSalesDamageQuantity()
    {
        return $this->salesDamageQuantity;
    }

    /**
     * @param float $salesDamageQuantity
     */
    public function setSalesDamageQuantity( $salesDamageQuantity)
    {
        $this->salesDamageQuantity = $salesDamageQuantity;
    }



}

