function jqueryTemporaryLoad() {

    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    $('#item').select2('open');
    function jsonResult(response){
        obj = JSON.parse(response);
        $('#invoiceItem').html(obj['invoiceItem']);
        $('#subTotal').html(obj['subTotal']);
    }

    /*$('form#itemForm').on('keypress', '.input', function (e) {

        if (e.which === 13) {
            var inputs = $(this).parents("form#itemForm").eq(0).find("input,select");
            var idx = inputs.index(this);
            if (idx == inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
            }
            switch (this.id) {
                case 'item':
                    $('#quantity').focus();
                    break;

                case 'quantity':
                    var qnt = $('#quantity').val();
                    if(qnt == "NaN" || qnt =="" ){
                        $('#quantity').focus();
                    }else{
                        $('#description').focus();
                    }
                    break;

                case 'description':
                    $('#addItem').click();
                    $('#purchaseItem_stockName').select2('open');
                    break;

            }
            return false;
        }
    });*/

    $('form#approveForm').submit(function(e){

        e.preventDefault();
        $.ajax({
            url         : $('form#approveForm').attr( 'action' ),
            type        : $('form#approveForm').attr( 'method' ),
            data        : new FormData($('form#approveForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                location.reload();
            }
        });
    });

    $('form#itemForm').submit(function(e){

        e.preventDefault();
        var item = $('#item').val();
        var quantity = $('#quantity').val();

        if(item === "NaN" || item ==="" ){
            $('#item').select2('open');
            //$.MessageBox("Please select sales output tax");
            return false;
        }
        if(quantity === '' || quantity === "NaN"){
            $('#quantity').focus();
            //$.MessageBox("Quantity must be required");
            return false;
        }

        $.ajax({
            url         : $('form#itemForm').attr( 'action' ),
            type        : $('form#itemForm').attr( 'method' ),
            data        : new FormData($('form#itemForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                setTimeout(jsonResult(response),100);
                $("#item").select2('open').prop('selectedIndex',0);
                // $('#item').prop('selectedIndex',0);
                $('form#itemForm')[0].reset();
            }
        });
    });

    $(document).on('change', '.quantity', function() {

        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var stockIn = parseFloat($('#stockIn-'+id).val());
        var salesPrice = parseFloat($('#salesPrice-'+id).val());
        var subTotal  = (quantity * salesPrice);
        $("#subTotal-"+id).html(subTotal);
        $.ajax({
            url: Routing.generate('procure_requisition_item_update'),
            type: 'POST',
            data:'item='+ id +'&quantity='+quantity +'&stockIn='+stockIn,
            success: function(response) {
                setTimeout(jsonResult(response),100);
            }

        })
    });

    $(document).on('change', '.storeRequisitionQuantity', function() {

        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var remainingQuantity = parseFloat($('#remainingQuantity-'+id).val());
        var remin  = (remainingQuantity - quantity);
        alert(remin);
        $("#remainQnt-"+id).html(remin);
    });

    $(document).on('click',".itemRemove", function (event) {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to delete this record?"
        }).done(function(){
            $.get(url, function( response ) {
                $(event.target).closest('tr').hide();
                setTimeout(jsonResult(response),100);
            });
        });
    });



   /* var form = $("#itemFormxx").validate({

        rules: {
            item: {required: true},
            quantity: {required: true},
            description: {required: true}
        },

        messages: {
            item:"Enter select item name",
            quantity:"Enter item quantity"
        },
        tooltip_options: {
            item: {trigger:'focus'},
            quantity: {trigger:'focus'}
        },

        submitHandler: function(form) {

            $.ajax({
                url         : $('form#itemForm').attr( 'action' ),
                type        : $('form#itemForm').attr( 'method' ),
                data        : new FormData($('form#itemForm')[0]),
                processData : false,
                contentType : false,
                success: function(response){
                    entityData(response);
                    $("#item").select2("val", "");
                    $('#itemForm')[0].reset();
                    //$('#addItem').html('<i class="icon-save"></i> Add').attr("disabled", false);
                }
            });
        }
    });*/


    $(".autocomplete2Item").select2({
        ajax: {
            url: Routing.generate('inv_stock_item_autocomplete'),
            data: function (params, page) {
                return {
                    q: params,
                    page_limit: 100
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            }
        },
        placeholder: 'Search for a add stock item',
        minimumInputLength: 1,
    });

    $(document).on('change', '.autocomplete2Item', function () {
        var employee = $(this).val();
        alert(employee);
    });
}






