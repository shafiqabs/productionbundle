<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ModuleProcess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class OrderDeliveryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderDelivery::class);
    }

    public function updateOrderDelivery($data)
    {

        /* @var $delivery OrderDelivery */
        $em = $this->_em;
        foreach ($data['deliverId'] as $key => $value){
            $delivery = $this->find($value);
            if(!empty($delivery) and !empty($delivery->getRequisitionOrderItems()) and $delivery->getDeliveryMethod()->getSlug() == "courier") {
                $courier = $em->getRepository(Particular::class)->find($data['courier']);
                $delivery->setCourier($courier);
                $delivery->setCnNo($data['cnNo']);
                $delivery->setComments($data['comments']);
                $em->persist($delivery);
                $em->flush();
            }elseif(!empty($delivery) and !empty($delivery->getRequisitionOrderItems()) and $delivery->getDeliveryMethod()->getSlug() == "hand-delivery") {
                $delivery->setReceiverName($data['receiverName']);
                $delivery->setReceiverDesignation($data['receiverDesignation']);
                $delivery->setReceiverAddress($data['receiverAddress']);
                $em->persist($delivery);
                $em->flush();
            }


        }
    }

    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['requisition_filter_form'])){

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $branch = isset($data['branch'])? $data['branch'] :'';
            $department = isset($data['department'])? $data['department'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? $data['requisitionNo'] :'';

            if(!empty($process)){
                $qb->andWhere($qb->expr()->like("e.process", "'%$process%'"));
            }
            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$requisitionNo%'"));
            }
            if(!empty($branch)){
                $qb->where('b.id = :branch')->setParameter('branch',$branch);
            }
            if(!empty($department)){
                $qb->where('d.id = :department')->setParameter('department',$department);
            }
            if(!empty($priroty)){
                $qb->where('p.id = :priority')->setParameter('priroty',$priroty);
            }
            if(!empty($requisitionType)){
                $qb->where('t.id = :requisitionType')->setParameter('requisitionType',$requisitionType);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }



    /**
     * @return Purchase[]
     */
    public function findLssdWithQuery( $config,$data = "" ): array
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.created as created','e.process as process','e.filename as filename','e.approved as approved');
        $qb->addSelect('ro.orderNo as orderNo','ro.id as requisitionOrderId');
        $qb->addSelect('r.requisitionNo as requisitionNo','r.id as requisitionId');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('dm.name as deliveryMethod');
        $qb->addSelect('t.name as requisitionType');
        $qb->join('e.requisitionOrder','ro');
        $qb->join('ro.requisition','r');
        $qb->leftJoin('e.deliveryMethod','dm');
        $qb->leftJoin('r.requisitionType','t');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('r.department','d');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }



}
