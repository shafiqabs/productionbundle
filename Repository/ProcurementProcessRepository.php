<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\ParticularType;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class ProcurementProcessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcurementProcess::class);
    }

    public function insertProcess(User $user ,Requisition $requisition)
    {
        $em = $this->_em;
        $exist  = $this->findOneBy(array('createdBy'=>$user, 'requisition'=>$requisition));
        if(empty($exist)){
            $entity = new ProcurementProcess();
            $entity->setConfig($requisition->getConfig());
            $entity->setRequisition($requisition);
            $entity->setCreatedBy($user);
            $entity->setProcess($requisition->getProcess());
            $entity->setComment($requisition->getComment());
            $em->persist($entity);
            $em->flush();
        }else{
            $exist->setComment($requisition->getComment());
            $em->persist($exist);
            $em->flush();
        }



    }

    public function insertApproveProcess(Requisition $requisition)
    {
        $em = $this->_em;
        $entity = new ProcurementProcess();
        $entity->setConfig($requisition->getConfig());
        $entity->setRequisition($requisition);
        $entity->setCreatedBy($user);
        $entity->setProcess($requisition->getProcess());
        $entity->setComment($requisition->getComment());
        $em->persist($entity);
        $em->flush();

    }

}
