<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Requisition::class);
    }


    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['requisition_filter_form'])){

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $branch = isset($data['branch'])? $data['branch'] :'';
            $department = isset($data['department'])? $data['department'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? $data['requisitionNo'] :'';

            if(!empty($process)){
                $qb->andWhere($qb->expr()->like("e.process", "'%$process%'"));
            }
            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$requisitionNo%'"));
            }
            if(!empty($branch)){
                 $qb->where('b.id = :branch')->setParameter('branch',$branch);
            }
            if(!empty($department)){
                 $qb->where('d.id = :department')->setParameter('department',$department);
            }
            if(!empty($priroty)){
                 $qb->where('p.id = :priority')->setParameter('priroty',$priroty);
            }
            if(!empty($requisitionType)){
                 $qb->where('t.id = :requisitionType')->setParameter('requisitionType',$requisitionType);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }


    /**
     * @return Purchase[]
     */
    public function findBySearchQuery( $config,User $user, $data = "" ): array
    {

        $workingArea = $user->getProfile()->getServiceMode()->getSlug();
        $roleId =  $user->getUserGroupRole()->getId();
        $process = $user->getUserGroupRole()->getProcess();

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total','e.process as process','e.filename as filename');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id = :initior')->setParameter('initior',$user->getId());
        if($workingArea == "branch"){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif($workingArea == "department"){
            $branchId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :branchId')->setParameter('branchId',$branchId);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    /**
     * @return Purchase[]
     */
    public function findByApproveQuery( $config, User $user, $data = "" ): array
    {

        $workingArea = $user->getProfile()->getServiceMode()->getSlug();
        $roleId =  $user->getUserGroupRole()->getId();
        $process = $user->getUserGroupRole()->getProcess();

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total','e.process as process','e.filename as filename');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.waitingProcess = :process')->setParameter('process',$process);
        if($workingArea == "branch"){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif($workingArea == "department"){
            $branchId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :branchId')->setParameter('branchId',$branchId);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function generateStoreRequisition(Requisition $requisition , $process = "")
    {
        $em =$this->_em;

        $module = $em->getRepository(AppModule::class)->findOneBy(array('slug' => $process));
        $terminal = $requisition->getCreatedBy()->getTerminal()->getId();

        /* @var $moduleProcess ModuleProcess */
        $moduleProcess = $em->getRepository(ModuleProcess::class)->findOneBy(array('terminal' =>$terminal,'module' => $module));

        foreach ($moduleProcess->getModuleProcessItems() as $row){
            $exist = $em->getRepository(ModuleProcess::class)->findOneBy(array('requisition'=>$requisition,'moduleProcessItem'=>$row));
            if(empty($exist)){
                $entity = new ProcurementProcess();
                $entity->setRequisition($requisition);
                $entity->setModuleProcessItem($row);
                $entity->setProcess($requisition->getProcess());
                $em->persist($entity);
                $em->flush();
            }
        }

    }

    public function requisitionItemHistory(Requisition $requisition)
    {
        $em =$this->_em;
        /* @var $item RequisitionItem */

        if(!empty($requisition->getRequisitionItems())){

            foreach ($requisition->getRequisitionItems() as $item){

                $entity = new RequisitionItemHistory();
                $entity->setRequisitionItem($item);
                $entity->setActualQuantity($item->getActualQuantity());
                $entity->setQuantity($item->getQuantity());
                $em->persist($entity);
                $em->flush();

            }
        }
    }


}
