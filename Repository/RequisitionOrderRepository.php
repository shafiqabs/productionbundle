<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ModuleProcess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequisitionOrder::class);
    }

    public function insertOrder(Requisition $requisition,$deliveryModes,$data)
    {


        $em = $this->_em;
        $entity = new RequisitionOrder();
        $entity->setConfig($requisition->getConfig());
        $entity->setRequisition($requisition);
        $entity->getProcess('New');
        $em->persist($entity);
        $em->flush();

        foreach ($deliveryModes as $mode){

            $particular = $em->getRepository(Particular::class)->find($mode['id']);
          //  $exist = $this->findOneBy(array('requisitionOrder'=> $entity,'deliveryMethod' => $particular));
            $delivery = new OrderDelivery();
            $delivery->setRequisitionOrder($entity);
            $delivery->setDeliveryMethod($particular);
            $delivery->setConfig($requisition->getConfig());
            $em->persist($delivery);
            $em->flush();

            foreach ($data['requisitionItem'] as $key => $item){

                if($delivery->getDeliveryMethod()->getId() == $data['deliveryMethod'][$key]){
                    $quantity = empty($data['quantity'][$key]) ? 0 :$data['quantity'][$key];
                    $purchaseItem = $em->getRepository(RequisitionItem::class)->find($item);
                    $orderItem = new RequisitionOrderItem();
                    $orderItem->setRequisitionOrder($entity);
                    $orderItem->setRequisitionItem($purchaseItem);
                    $orderItem->setStock($purchaseItem->getStock());
                    $orderItem->setOrderDelivery($delivery);
                    $orderItem->setQuantity($quantity);
                    $orderItem->setPrice($purchaseItem->getPrice());
                    $subTotal = (float)($orderItem->getPrice() * $orderItem->getQuantity());
                    $orderItem->setSubTotal($subTotal);
                    $em->persist($orderItem);
                    $em->flush();
                }
            }
            $em->getRepository(RequisitionOrderItem::class)->getSumSubTotal($delivery);

        }


        return $entity;
    }

    public function requisitionItemHistory(Requisition $requisition)
    {
        $em =$this->_em;
        /* @var $item RequisitionItem */

        if(!empty($requisition->getRequisitionItems())){

            foreach ($requisition->getRequisitionItems() as $item){

                $entity = new RequisitionItemHistory();
                $entity->setRequisitionItem($item);
                $entity->setActualQuantity($item->getActualQuantity());
                $entity->setQuantity($item->getQuantity());
                $em->persist($entity);
                $em->flush();

            }
        }
    }

    public function orderItemStockItemUpdate(Stock $stock)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requsitionOrderItem', 'mp');
        $qb->select('SUM(e.quantity) AS quantity');
        $qb->where('e.stock = :stock')->setParameter('stock', $stock->getId());
     //   $qb->andWhere('mp.process = :process')->setParameter('process', 'Approved');
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['quantity'];
    }


}
