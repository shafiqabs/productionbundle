<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller;

use App\Entity\Admin\AppModule;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Form\RequisitionApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procure/requisition")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_requisition")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());

        $purchase = new Requisition();
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , $purchase,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);

        $data = $_REQUEST;
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/requisition/index.html.twig',
            [
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/new", methods={"GET"}, name="procure_requisition_new" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function new(Request $request,TranslatorInterface $translator ,ProcurementRepository $procurementRepository, RequisitionRepository $requisitionRepository, ParticularRepository $particularRepository, ModuleProcessItemRepository $processItemRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $types = $particularRepository->getChildRecords($config->getId(),'requisition-type');
        $type = isset($_REQUEST['type']) and !empty($_REQUEST['type']) ? $_REQUEST['type'] :"";
        if (!empty($type)) {
            $type = $_REQUEST['type'];
            $rType = $particularRepository->findOneBy(array('config'=>$config,'slug' => $type));
            $entity = new Requisition();
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $entity->setRequisitionType($rType);
            if($type == "store-requisition"){
                $operationalRole = $processItemRepository->checkingOperationalRole($this->getUser(),"store-requisition");
                $entity->setCurrentRole($operationalRole['currentRole']);
                $entity->setProcess($operationalRole['currentRole']->getBundleRoleGroup()->getProcess());
            }
            $user = $this->getUser();
            $entity->setCreatedBy($user);
            if($user->getProfile() and !empty($user->getProfile()->getBranch())){
                $entity->setBranch($user->getProfile()->getBranch());
            }elseif ($user->getProfile() and !empty($user->getProfile()->getDepartment())){
                $entity->setDepartment($user->getProfile()->getDepartment());
            }
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            if($type == "store-requisition"){
                return $this->redirectToRoute('procure_store_requisition_edit',array('id'=>$entity->getId()));
            }else{
                return $this->redirectToRoute('procure_requisition_edit',array('id'=>$entity->getId()));
            }
        }
        return $this->render('@TerminalbdProcurement/requisition/new.html.twig',['types'=>$types]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_requisition_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id,TranslatorInterface $translator, RequisitionRepository $repository): Response
    {
        /* @var $entity Requisition */
        $entity = $repository->find($id);
        if($entity->getRequisitionType()->getSlug() == "store-requisition"){
            return $this->redirectToRoute('procure_store_requisition_edit',array('id'=>$entity->getId()));
        }else{
            return $this->redirectToRoute('procure_requisition_edit',array('id'=>$entity->getId()));
        }
    }




     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_requisition_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, RequisitionRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdProcurement/requisition/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_requisition_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_requisition_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id, SettingRepository $repository): Response
    {

        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal);
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        /* @var $entity Particular */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));

        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/meta-delete", methods={"GET"}, name="procure_requisition_meta_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function metaDelete($id, ItemKeyValueRepository $keyValueRepository): Response
    {
        $entity = $keyValueRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        return new Response('Success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="procure_requisition_data_table" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function dataTable(Request $request, ProcurementRepository $procurementRepository , RequisitionRepository $repository)
    {

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entities = $repository->findBy(array('config'=>$config,'createdBy'=>$this->getUser()));
        $iTotalRecords = $repository->count(array('config'=> $config,'createdBy'=>$this->getUser()));

        $i = 1;
        $records = array();
        $records["data"] = array();

        /* @var $post Requisition */

        foreach ($entities as $post):

            $branch = empty($post->getBranch()) ? '' : $post->getBranch()->getName();
            $department = empty($post->getDepartment()) ? '' : $post->getDepartment()->getName();
            $requisitionLocation = (empty($branch)) ? $department : $branch;

            $priority = empty($post->getPriority()) ? '' : $post->getPriority()->getName();
            $active = empty($post->isStatus()) ? '' : "checked";
            $status ="<input class='status' data-action='{$this->generateUrl('procure_requisition_status',array('id'=>$post->getId()))}' type='checkbox' {$active} data-toggle='toggle' data-size='xs' data-style='slow' data-offstyle='warning' data-onstyle='info' data-on='Enabled'  data-off='Disabled'>";
            $expected = empty($post->getExpectedDate()) ? '' : $post->getExpectedDate()->format('d-m-y');
            $action = "";
            if($post->getRequisitionType()->getSlug() == "store-requisition"){
                $action = "<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('procure_store_requisition_edit',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn btn-mini blue-bg white-font entity-show' data-action='{$this->generateUrl('procure_requisition_show',array('id'=>$post->getId()))}' href='javascript:' data-title='{$post->getRequisitionNo()}' id='postView-{$post->getId()}' ><i class='feather icon-eye'></i> Show</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post->getId()}' href='javascript:' data-action='{$this->generateUrl('procure_requisition_delete',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-trash-2'></i></a>";
            }else{
                $action = "<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('procure_requisition_edit',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn btn-mini blue-bg white-font entity-show' data-action='{$this->generateUrl('procure_requisition_show',array('id'=>$post->getId()))}' href='javascript:' data-title='{$post->getRequisitionNo()}' id='postView-{$post->getId()}' ><i class='feather icon-eye'></i> Show</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post->getId()}' href='javascript:' data-action='{$this->generateUrl('procure_requisition_delete',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-trash-2'></i></a>";
            }

          //  $origin = new \DateTime('2009-10-11');

            $origin = $post->getCreated();
            $target = new \DateTime('now');
            $interval = $origin->diff($target);
            $intDate = $interval->format('Days %a Hours %H');

            $records["data"][] = array(

                $id                 = $i,
                $created            = "<b>".$post->getCreated()->format('d-m-yy')."</b><br> \n".$intDate,
                $pr                 = "PR-{$post->getRequisitionNo()}",
              //  $expected           = $expected,
             //   $user               = $post->getCreatedBy()->getName(),
                $type               = $post->getRequisitionType()->getName(),
                $requisitionLocation             = $requisitionLocation,
                $priority           = $priority,
                $process            = $post->getProcess(),
                $status             = $status,
                $action             = $action);
            $i++;
        endforeach;
        return new JsonResponse($records);
    }


    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/download-attachment", methods={"GET","POST"}, name="procure_requisition_download_attachment", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */


    public function downloadAttachFileAction(Request $request , Requisition  $requisition)
    {


        $path = $request->headers->get('host')."/public/uploads/";
        $file = $path.$requisition->getFilename();
        if (!empty($requisition->getFilename()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
        exit;
    }




}
