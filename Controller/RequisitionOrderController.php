<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Service\FileUploader;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Form\LssdFilterFormType;
use Terminalbd\ProcurementBundle\Form\OrderReceiveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionOrderFormType;
use Terminalbd\ProcurementBundle\Repository\OrderDeliveryRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procure/requisition/order")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionOrderController extends AbstractController
{

    /**
     * @Route("/approval", methods={"GET", "POST"}, name="procure_requisition_order")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function approve(Request $request, TranslatorInterface $translator, ProcurementRepository $procurementRepository , ProcurementProcessRepository $processRepository,RequisitionRepository $repository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $profile = $this->getUser()->getProfile();
        $workingArea = $profile->getServiceMode()->getSlug();

        $roleId =  $this->getUser()->getUserGroupRole()->getId();
        $process = $this->getUser()->getUserGroupRole()->getProcess();

        if($workingArea == "branch"){
            $entities = $repository->findBy(array('config'=>$config,'branch' => $profile->getBranch(),'waitingProcess' => $process));
        }elseif($workingArea == "department"){
            $entities = $repository->findBy(array('config'=>$config,'department' => $profile->getDepartment(),'waitingProcess' => $process));
        }
        return $this->render('@TerminalbdProcurement/requisition-order/index.html.twig',['entities' => $entities]);
    }

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/lssd", methods={"GET", "POST"}, name="procure_requisition_order_lssd")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function lssd(Request $request, TranslatorInterface $translator,  OrderDeliveryRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());

        $purchase = new OrderDelivery();
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(LssdFilterFormType::class , $purchase,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $search = $repository->findLssdWithQuery($config,$data);
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/requisition-order/lssd.html.twig',
            [
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/lssd-approve", methods={"GET", "POST"}, name="procure_requisition_order_lssd_approve")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function lssdApprove(Request $request,OrderDeliveryRepository $repository): Response
    {
        $approves = $_REQUEST['approves'];
        $new_arr = array_map('trim', explode(',', $approves));
        /* @var $entity OrderDelivery */
        foreach ($new_arr as $item){
            $entity = $repository->find($item);
            $entity->setApproved(true);
            $entity->setApprovedBy($this->getUser());
            $this->getDoctrine()->getManager()->flush();
        }
        return new Response("success");
    }



   /**
     * @Route("/order-delivery", methods={"GET", "POST"}, name="procure_requisition_order_delivery")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function orderDelivery(Request $request, TranslatorInterface $translator, ProcurementRepository $procurementRepository, OrderDeliveryRepository $deliveryRepository): Response
    {

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $profile = $this->getUser()->getProfile();
        $workingArea = $profile->getServiceMode()->getSlug();
        if($workingArea == "branch"){
            $entities = $deliveryRepository->findBy(array('config'=>$config),array('created'=>"DESC"));
        }elseif($workingArea == "department"){
            $entities = $deliveryRepository->findBy(array('config'=>$config),array('created'=>"DESC"));
        }else{
        }
        $entities = $deliveryRepository->findBy(array('config'=>$config),array('created'=>"DESC"));
        return $this->render('@TerminalbdProcurement/requisition-order/order-delivery.html.twig',['entities' => $entities]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/receive-print", methods={"GET","POST"}, name="procure_requisition_order_print" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function print(Request $request,$id,OrderDeliveryRepository $repository,ProcurementRepository $procurementRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        return $this->render('@TerminalbdProcurement/requisition-order/print.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/receive-print", methods={"GET","POST"}, name="procure_requisition_store_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(Request $request,$id,OrderDeliveryRepository $repository,ProcurementRepository $procurementRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        return $this->render('@TerminalbdProcurement/requisition-order/print.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/receive", methods={"GET","POST"}, name="procure_requisition_order_receive" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function receive(Request $request , $id, TranslatorInterface $translator , OrderDeliveryRepository $repository,ProcurementRepository $procurementRepository, FileUploader $fileUploader ): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $form = $this->createForm(OrderReceiveFormType::class , $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $this->getDoctrine()->getManager()->flush();
            $brochureFile = $form->get('attachFile')->getData();
            if ($brochureFile) {
                $brochureFileName = $fileUploader->upload($brochureFile);
                $entity->setFilename($brochureFileName);
            }
            $entity->setProcess('Close');
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_order_delivery');
        }
        return $this->render('@TerminalbdProcurement/requisition-order/receive.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/procurement-store", methods={"GET", "POST"}, name="procure_requisition_order_approval")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function procurementStore(Request $request, TranslatorInterface $translator, ProcurementRepository $procurementRepository , ProcurementProcessRepository $processRepository,RequisitionRepository $repository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $profile = $this->getUser()->getProfile();
        $workingArea = $profile->getServiceMode()->getSlug();

        $roleId =  $this->getUser()->getUserGroupRole()->getId();
        $process = $this->getUser()->getUserGroupRole()->getProcess();

        $entities = $repository->findBy(array('config'=>$config,'waitingProcess' => $process));
        return $this->render('@TerminalbdProcurement/requisition-order/index.html.twig',['entities' => $entities]);

    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/order-process", methods={"GET","POST"}, name="procure_requisition_order_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function storeProcess(Request $request , $id,TranslatorInterface $translator, RequisitionRepository $repository,ParticularRepository $particularRepository , ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository, RequisitionOrderRepository $requisitionOrderRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $deliveryModes = $particularRepository->getChildRecords($config,"delivery-mode");
        $form = $this->createForm(RequisitionApproveFormType::class , $entity,array('config'=>$config,'particularRepo' => $particularRepository));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            if($entity->getRequisitionType()->getSlug() == "store-requisition"){
                $operationalRole = $processItemRepository->checkingOperationalRole($this->getUser(),"store-requisition");
                $entity->setCurrentRole($operationalRole['currentRole']);
                $entity->setProcess($operationalRole['currentRole']->getBundleRoleGroup()->getProcess());
                if(empty($operationalRole['waitingRole'])){
                    $entity->setWaitingRole(NULL);
                    //$entity->setWaitingProcess($operationalRole['currentRole']->getModuleProcess()->getEndStatus());
                }
            }
            $requisitionOrder = $requisitionOrderRepository->insertOrder($entity,$deliveryModes,$data);
            $this->getDoctrine()->getManager()->flush();
            $processRepository->insertProcess($this->getUser(),$entity);
            $repository->requisitionItemHistory($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_order_deliver',array('id'=>$requisitionOrder->getId()));
        }

        return $this->render('@TerminalbdProcurement/requisition-order/storeProcess.html.twig', [
            'entity' => $entity,
            'deliveryModes' => $deliveryModes,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/order-deliver", methods={"GET","POST"}, name="procure_requisition_order_deliver" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function orderDeliverProcess(Request $request , $id, TranslatorInterface $translator, RequisitionOrderRepository $repository, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository, ParticularRepository $particularRepository, StockRepository $stockRepository, OrderDeliveryRepository $deliveryRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $form = $this->createForm(RequisitionOrderFormType::class , $entity,array('config'=>$config));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();

            /*if($entity->getRequisitionType()->getSlug() == "store-requisition"){
                $operationalRole = $processItemRepository->checkingOperationalRole($this->getUser(),"store-requisition");
                $entity->setCurrentRole($operationalRole['currentRole']);
                $entity->setProcess($operationalRole['currentRole']->getBundleRoleGroup()->getProcess());
                if(empty($operationalRole['waitingRole'])){
                    $entity->setWaitingRole(NULL);
                    $entity->setWaitingProcess($operationalRole['currentRole']->getModuleProcess()->getEndStatus());
                }
            }*/

            $this->getDoctrine()->getManager()->flush();
          //  $processRepository->insertProcess($this->getUser(),$entity);
            $deliveryRepository->updateOrderDelivery($data);
            $stockRepository->processRequisitionOrderUpdateQnt($entity);
            //$repository->requisitionItemHistory($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_order');
        }
        $couriers = $particularRepository->getChildRecords($config,'courier-service');
        return $this->render('@TerminalbdProcurement/requisition-order/orderProcess.html.twig', [
            'entity' => $entity,
            'couriers' => $couriers,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/approval-data-table", methods={"GET", "POST"}, name="procure_approval_requisition_data_table" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function approvalDataTable(Request $request, ProcurementRepository $procurementRepository , RequisitionRepository $repository)
    {

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $profile = $this->getUser()->getProfile();
        $workingArea = $profile->getServiceMode()->getSlug();
        if($workingArea == "branch"){
           // echo $profile->getBranch()->getId();
            $entities = $repository->findBy(array('config'=>$config,'branch' => $profile->getBranch(),'process' => "In-progress"));
            $iTotalRecords = $repository->count(array('config'=>$config,'branch' => $profile->getBranch(),'process' => "In-progress"));
        }elseif($workingArea == "department"){
            $entities = $repository->findBy(array('config'=>$config,'branch' => $profile->getBranch(),'process' => "In-progress"));
            $iTotalRecords = $repository->count(array('config'=>$config,'branch' => $profile->getBranch(),'process' => "In-progress"));
        }

        $i = 1;
        $records = array();
        $records["data"] = array();

        /* @var $post Requisition */

        foreach ($entities as $post):

            $origin = $post->getCreated();
            $target = new \DateTime('now');
            $interval = $origin->diff($target);
            $intDate = $interval->format('Days %a Hours %H');

            $viewUrl = $this->generateUrl('procure_requisition_process',array('id'=>$post->getId()));

            $branch = empty($post->getBranch()) ? '' : $post->getBranch()->getName();
            $department = empty($post->getDepartment()) ? '' : $post->getDepartment()->getName();

            $requisitionLocation = (empty($branch)) ? $department : $branch;

            $priority = empty($post->getPriority()) ? '' : $post->getPriority()->getName();
            $active = empty($post->isStatus()) ? '' : "checked";
            $status ="<input class='status' data-action='{$this->generateUrl('procure_requisition_status',array('id'=>$post->getId()))}' type='checkbox' {$active} data-toggle='toggle' data-size='xs' data-style='slow' data-offstyle='warning' data-onstyle='info' data-on='Enabled'  data-off='Disabled'>";
            $expected = empty($post->getExpectedDate()) ? '' : $post->getExpectedDate()->format('d-m-y');
            $action = "";
            $action = "<a class='btn btn-mini purple-bg white-font' data-action='{$viewUrl}' href='?process=postView-{$post->getId()}&check=process#modal' id='postView-{$post->getId()}' ><i class='feather icon-aperture'></i> Approve</a>";
            $records["data"][] = array(

                $id                 = $i,
                $created            = "<b>".$post->getCreated()->format('d-m-yy')."</b><br> \n".$intDate,
                $pr                 = "PR-{$post->getRequisitionNo()}",
                $expected           = $expected,
                $user               = "{$post->getCreatedBy()->getProfile()->getEmployeeId()} - {$post->getCreatedBy()->getName()}",
                $type               = $post->getRequisitionType()->getName(),
                $branch             = $requisitionLocation,
                $priority           = $priority,
                $process            = $post->getProcess(),
                $action             = $action);
            $i++;
        endforeach;
        return new JsonResponse($records);
    }





}
