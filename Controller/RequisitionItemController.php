<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use ContainerBlo4KOZ\getRequisitionItemRepositoryService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procurement/requisition-item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionItemController extends AbstractController
{

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-item", methods={"GET","POST"}, name="procure_requisition_item_add" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function addItem(Request $request, Requisition $entity, RequisitionRepository $requisitionRepository , RequisitionItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $purchaseData = $data['requisition_item_form'];
        $itemRepository->insertStoreItem($entity,$purchaseData);
        $itemRepository->getItemSummary($entity);
        $reponse = $this->returnResultData($requisitionRepository,$entity->getId());
        return new Response(json_encode($reponse));
    }

    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{entity}/{id}/delete-item", methods={"GET"}, name="procure_requisition_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteItem($entity , $id, ProcurementRepository $procurementRepository, RequisitionRepository $requisitionRepository, RequisitionItemRepository $itemRepository): Response
    {

        /* @var $config  ProcurementRepository */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);

        $requisition = $requisitionRepository->findOneBy(array('config' => $config,'id' => "{$entity}"));
        $item = $itemRepository->findOneBy(array('requisition' => $requisition ,'id' => "{$id}"));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        $itemRepository->getItemSummary($requisition);
        $return = $this->returnResultData($requisitionRepository,$requisition->getId());
        return new Response(json_encode($return));
    }

    /**
     * Update a RequisitionItem entity.
     * @Route("/update-item", methods={"GET","POST"}, name="procure_requisition_item_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function updateItem(Request $request,RequisitionRepository $requisitionRepository,RequisitionItemRepository $itemRepository): Response
    {

        /* @var $item  RequisitionItem */
        $data = $request->request->all();
        $id = $data['item'];
        $item = $itemRepository->find($id);
        $itemRepository->updateStoreItem($item,$data['quantity'],$data['stockIn']);
        $itemRepository->getItemSummary($item->getRequisition());
        $return = $this->returnResultData($requisitionRepository,$item->getRequisition()->getId());
        return new Response(json_encode($return));
    }

    public function returnResultData(RequisitionRepository $requisitionRepository ,$requisition){

        $entity = $requisitionRepository->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/requisition/requisitionItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }


}
