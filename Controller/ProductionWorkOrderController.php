<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProductionBundle\Controller;


use App\Entity\Application\Production;
use App\Repository\Core\SettingTypeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\ProductionBundle\Entity\InvoiceKeyValue;
use Terminalbd\ProductionBundle\Entity\ProductionItem;
use Terminalbd\ProductionBundle\Entity\ProductionWorkOrder;
use Terminalbd\ProductionBundle\Entity\ProductionWorkOrderItem;
use Terminalbd\ProductionBundle\Form\WorkOrderFormType;
use Terminalbd\ProductionBundle\Repository\ItemRepository;

/**
 * @Route("/production/work-order")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProductionWorkOrderController extends AbstractController
{

    /**
     * @Route("/", methods={"GET"}, name="production_workorder")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     */
    public function index(Request $request): Response
    {

         return $this->render('@TerminalbdProduction/workorder/index.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     * @Route("/new", methods={"GET", "POST"}, name="production_workorder_create")
     */

    public function create(Request $request): Response
    {
        $entity = new ProductionWorkOrder();
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $entity->setConfig($config);
        $issueDate = new \DateTime('now');
        $entity->setIssuedate($issueDate);
        $entity->setUpdated($issueDate);
        $entity->setProcess('created');
        $em->persist($entity);
        $em->flush();
        return $this->redirectToRoute('production_workorder_edit',['id'=> $entity->getId()]);

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="production_workorder_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     */
    public function edit(Request $request, ProductionWorkOrder $entity): Response
    {
        $terminal = $this->getUser()->getTerminal();
        if(in_array($entity->getProcess(),array("checked","approved"))){
            return $this->redirectToRoute('production_workorder_show',['id'=> $entity->getId()]);
        }
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $products = $this->getDoctrine()->getRepository("TerminalbdProductionBundle:ProductionItem")->findProducts($config);
        $form = $this->createForm(WorkOrderFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $data = $request->request->all();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->upload();
            $this->getDoctrine()->getManager()->flush();
            $this->getDoctrine()->getRepository('TerminalbdProductionBundle:InvoiceKeyValue')->insertWorkOrderKeyValue($entity,$data);
            if($entity->getProcess() == "done"){
                return $this->redirectToRoute('production_workorder_show',['id'=> $entity->getId()]);
            }
            return $this->redirectToRoute('production_workorder_edit',['id'=> $entity->getId()]);
        }
        return $this->render('@TerminalbdProduction/workorder/new.html.twig', [
            'actionUrl' => $this->generateUrl('production_workorder_edit',array('id'=> $entity->getId())),
            'dataAction' => $this->generateUrl('production_workorder_ajax',array('id'=> $entity->getId())),
            'entity' => $entity,
            'products' => $products,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-submit",methods={"GET", "POST"}, name="production_workorder_ajax")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     */

    public function ajaxSubmit(Request $request, ProductionWorkOrder $entity): Response
    {
        $data = $_REQUEST;
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $form = $this->createForm(WorkOrderFormType::class, $entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
        }
        return new Response('success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{meta}/delete-invoice-meta", methods={"GET"}, name="production_workorder_meta")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     */
    public function deleteInvoiceMeta(InvoiceKeyValue $meta): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($meta);
        $em->flush();
        return new Response('success');
    }


    /**
     * Check if mobile available for registering
     * @Route("/{id}/work-order-item", methods={"POST"}, name="production_workorder_item_create")
     * @param   string
     * @return  bool
     */
    function itemInsertAjax(ProductionWorkOrder $workOrder , Request $reques) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionWorkOrderItem')->insertUpdate($workOrder,$data);
        return new Response('success');

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/show",methods={"GET", "POST"}, name="production_workorder_show")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     */

    public function show(Request $request, ProductionWorkOrder $entity): Response
    {
        $data = $_REQUEST;
        $em = $this->getDoctrine()->getManager();
        return new Response('success');
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/{mode}/print",methods={"GET", "POST"}, name="production_workorder_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     */

    public function print(Request $request, ProductionWorkOrder $entity): Response
    {
        $data = $_REQUEST;
        $em = $this->getDoctrine()->getManager();
        return new Response('success');
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="production_workorder_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionWorkOrder::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/element-delete", methods={"GET"}, name="production_workorder_item_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     */
    public function elementDelete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionWorkOrderItem::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Group Process a Setting entity.
     *
     * @Route("/{mode}/process-group", methods={"GET"}, name="production_workorder_process_group")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     */
    public function processGroup($mode): Response
    {
        $em = $this->getDoctrine()->getManager();
        $ids = explode(',',$_REQUEST['ids']);
        foreach ($ids as $key => $id):

            $post = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:ProductionIssue")->find($id);
            $item = $post->getItem();
            $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
            $post->setProcess($mode);
            $em->flush();
            if($post->getProcess() == 'approved'){
                $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionInventory')->insertProductionInventory($config,$item);
            }
        endforeach;
        return new Response('Success');

    }

    /**
     * Process a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET"}, name="production_workorder_process")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_WORKORDER')")
     */
    public function process($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionWorkOrder::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionInventory')->insertProductionInventory($config,$post->getItem());
        $post->setProcess('approved');
        $em->flush();
        return $this->redirectToRoute('production_issue');

    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="production_workorderdata_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_workorderSALES')")
     */

    public function inventoryDataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(ProductionWorkOrder::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionWorkOrder')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post ProductionWorkOrder */

        foreach ($result as $post):

            $deleteUrl = $this->generateUrl('production_workorder_delete', array('id' => $post['id']));
            $viewUrl = $this->generateUrl('production_workorder_show', array('id' => $post['id']));
            $processUrl = $this->generateUrl('production_workorder_process', array('id' => $post['id']));
            $checkBox = "";
            if ($post['process'] != "approved") {
                $checkBox = "<input class='process' type='checkbox' name='process' id='process' value='{$post['id']}' /> ";
            }
            $action = '';

            if ($post['process'] == "created"){
                $action = "<a  data-action='{$viewUrl}' class='btn approve green-bg white-font btn-mini' data-id='{$post['id']}' href='javascript:'><i class='feather icon-eye'></i></a>
<a  data-action='{$processUrl}' class='btn approve indigo-bg white-font btn-mini' data-id='{$post['id']}' href='javascript:'><i class='feather icon-check'></i>Check</a>
 <a  data-action='{$deleteUrl}' class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:'><i class='fa fa-remove'></i></a>";
            }elseif ($post['process'] == "checked"){
                $action = "<a  data-action='{$viewUrl}' class='btn approve green-bg white-font btn-mini' data-id='{$post['id']}' href='javascript:'><i class='feather icon-eye'></i></a><a  data-action='{$processUrl}' class='btn approve green-bg white-font btn-mini' data-id='{$post['id']}' href='javascript:'><i class='feather icon-check-square'></i>Approve</a>
 <a  data-action='{$deleteUrl}' class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:'><i class='fa fa-remove'></i></a>";
            }
            $created = $post['created']->format('d-m-Y');
            $issueDate = $post['issueDate']->format('d-m-Y');

            $records["data"][] = array(
                $checkbox           = $checkBox,
                $id                 = $i,
                $created            = $created,
                $issueDate          = $issueDate,
                $requsitionNo       = $post['requsitionNo'],
                $invoice            = $post['invoice'],
                $process            = ucfirst($post['process']),
                $action             ="<div class='btn-group'>{$action}</div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }


}
