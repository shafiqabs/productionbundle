<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Form\RequisitionApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procure/requisition/process")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionProcessController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/approval", methods={"GET", "POST"}, name="procure_requisition_approval")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function approve(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());

        $purchase = new Requisition();
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , $purchase,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findByApproveQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findByApproveQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/requisition/approval.html.twig',
            [
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_requisition_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , $id, RequisitionRepository $repository,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository, RequisitionItemRepository $itemRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $form = $this->createForm(RequisitionApproveFormType::class , $entity,array('config'=>$config,'particularRepo' => $particularRepository));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $em = $this->getDoctrine()->getManager();
            $entity->setComment($entity->getContent());
            $entity->setContent(NULL);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);

            }else{
                if($entity->getRequisitionType()->getSlug() == "store-requisition"){
                    $operationalRole = $processItemRepository->checkingOperationalRole($this->getUser(),"store-requisition");
                    $entity->setCurrentRole($operationalRole['currentRole']);
                    $entity->setProcess($operationalRole['currentRole']->getBundleRoleGroup()->getProcess());
                    $entity->setWaitingRole($operationalRole['waitingRole']);
                    $entity->setWaitingProcess($operationalRole['waitingRole']->getBundleRoleGroup()->getProcess());
                }
                $em->persist($entity);
                $em->flush();
                $itemRepository->updateApproveQuantity($data);
                $repository->requisitionItemHistory($entity);
                $itemRepository->getItemSummary($entity);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            $processRepository->insertProcess($this->getUser(),$entity);
            return $this->redirectToRoute('procure_requisition_approval');
        }
        
        return $this->render('@TerminalbdProcurement/requisition/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/procurement-store", methods={"GET", "POST"}, name="procure_requisition_store_approval")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function procurementStore(Request $request, TranslatorInterface $translator, ProcurementRepository $procurementRepository , ProcurementProcessRepository $processRepository,RequisitionRepository $repository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $profile = $this->getUser()->getProfile();
        $workingArea = $profile->getServiceMode()->getSlug();

        $roleId =  $this->getUser()->getUserGroupRole()->getId();
        $process = $this->getUser()->getUserGroupRole()->getProcess();
        $entities = $repository->findBy(array('config'=>$config,'waitingProcess' => $process),array('created'=>"DESC"));
        return $this->render('@TerminalbdProcurement/requisition-order/index.html.twig',['entities' => $entities]);

    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/store-process", methods={"GET","POST"}, name="procure_requisition_store_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function storeProcess(Request $request , $id, RequisitionRepository $repository,ParticularRepository $particularRepository , ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $deliveryModes = $particularRepository->getChildRecords($config,"delivery-mode");
        return $this->render('@TerminalbdProcurement/requisition/storeProcess.html.twig', [
            'entity' => $entity,
            'deliveryModes' => $deliveryModes,
        ]);
    }


    /**
     * @Route("/approval-data-table", methods={"GET", "POST"}, name="procure_approval_requisition_data_table" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function approvalDataTable(Request $request, ProcurementRepository $procurementRepository , RequisitionRepository $repository)
    {

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $profile = $this->getUser()->getProfile();
        $workingArea = $profile->getServiceMode()->getSlug();
        if($workingArea == "branch"){
           // echo $profile->getBranch()->getId();
            $entities = $repository->findBy(array('config'=>$config,'branch' => $profile->getBranch(),'process' => "In-progress"));
            $iTotalRecords = $repository->count(array('config'=>$config,'branch' => $profile->getBranch(),'process' => "In-progress"));
        }elseif($workingArea == "department"){
            $entities = $repository->findBy(array('config'=>$config,'branch' => $profile->getBranch(),'process' => "In-progress"));
            $iTotalRecords = $repository->count(array('config'=>$config,'branch' => $profile->getBranch(),'process' => "In-progress"));
        }


        $i = 1;
        $records = array();
        $records["data"] = array();

        /* @var $post Requisition */

        foreach ($entities as $post):

            $origin = $post->getCreated();
            $target = new \DateTime('now');
            $interval = $origin->diff($target);
            $intDate = $interval->format('Days %a Hours %H');

            $viewUrl = $this->generateUrl('procure_requisition_process',array('id'=>$post->getId()));

            $branch = empty($post->getBranch()) ? '' : $post->getBranch()->getName();
            $department = empty($post->getDepartment()) ? '' : $post->getDepartment()->getName();

            $requisitionLocation = (empty($branch)) ? $department : $branch;

            $priority = empty($post->getPriority()) ? '' : $post->getPriority()->getName();
            $active = empty($post->isStatus()) ? '' : "checked";
            $status ="<input class='status' data-action='{$this->generateUrl('procure_requisition_status',array('id'=>$post->getId()))}' type='checkbox' {$active} data-toggle='toggle' data-size='xs' data-style='slow' data-offstyle='warning' data-onstyle='info' data-on='Enabled'  data-off='Disabled'>";
            $expected = empty($post->getExpectedDate()) ? '' : $post->getExpectedDate()->format('d-m-y');
            $action = "";
            $action = "<a class='btn btn-mini purple-bg white-font' data-action='{$viewUrl}' href='?process=postView-{$post->getId()}&check=process#modal' id='postView-{$post->getId()}' ><i class='feather icon-aperture'></i> Approve</a>";
            $records["data"][] = array(

                $id                 = $i,
                $created            = "<b>".$post->getCreated()->format('d-m-yy')."</b><br> \n".$intDate,
                $pr                 = "PR-{$post->getRequisitionNo()}",
                $expected           = $expected,
                $user               = "{$post->getCreatedBy()->getProfile()->getEmployeeId()} - {$post->getCreatedBy()->getName()}",
                $type               = $post->getRequisitionType()->getName(),
                $branch             = $requisitionLocation,
                $priority           = $priority,
                $process            = $post->getProcess(),
                $action             = $action);
            $i++;
        endforeach;
        return new JsonResponse($records);
    }





}
