<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProductionBundle\Controller;

use App\Entity\Application\Inventory;
use App\Entity\Application\Production;
use App\Entity\Core\Setting;
use App\Entity\Core\SettingType;
use App\Form\Core\SettingFormType;
use App\Repository\Core\SettingTypeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\ProductionBundle\Entity\ProductionElement;
use Terminalbd\ProductionBundle\Entity\ProductionInventory;
use Terminalbd\ProductionBundle\Entity\ProductionIssue;
use Terminalbd\ProductionBundle\Entity\ProductionItem;
use Terminalbd\ProductionBundle\Entity\ProductionItemAmendment;
use Terminalbd\ProductionBundle\Repository\ItemRepository;

/**
 * @Route("/production/build/version")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProductionBuildAmendmentController extends AbstractController
{


    /**
     * @Security("is_granted('ROLE_DOMAIN')")
     * @Route("/{id}/", methods={"GET", "POST"}, name="production_build_amendment")
     */
    public function amendment(ProductionItem $item , Request $request): Response
    {
        $user = $this->getUser();
        /* @var $exist ProductionItemAmendment */
        $exist = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionItemAmendment')->findInitiateAmendment($item);
        if($exist){
            $amendment = $exist;
        }else{
            $amendment = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionItemAmendment')->insertAmendmentProduction($user,$item);
        }
        return $this->redirectToRoute('production_build_amendment_generate',['id'=> $amendment->getId()]);

    }

    /**
     * @Security("is_granted('ROLE_DOMAIN')")
     * @Route("/{id}/generate", methods={"GET", "POST"}, name="production_build_amendment_generate")
     */
    public function amendmentUpdate(ProductionItemAmendment $item , Request $request): Response
    {
        if($item->getProcess() == 'approved'){
            return $this->redirectToRoute('production_build_amendment_show',['id'=> $item->getId()]);
        }
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $modes = array("raw-material","consumable","stockable","assembled","finish-goods");
        $products = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Item")->modeWiseStockItem($config,$modes);
        return $this->render('@TerminalbdProduction/production-item/amendment.html.twig', [
            'item'      => $item,
            'products'  => $products,
        ]);
    }


     /**
     * Search a Setting entity.
     * @Route("/{id}/show", methods={"GET"}, name="production_build_amendment_show")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION')")
     */
    public function showAmendment(ProductionItemAmendment $amendment)
    {
        return $this->render('@TerminalbdProduction/production-item/show.html.twig', [
            'item'      => $amendment,
        ]);
    }

    /**
     * Search a Setting entity.
     * @Route("/{id}/recipe-item-search", methods={"GET"}, name="production_build_item_search")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION')")
     */

    public function particularSearch(Item $particular)
    {
        $unit = !empty($particular->getUnit() && !empty($particular->getUnit()->getName())) ? $particular->getUnit()->getName():'Unit';
        return new Response(json_encode(array('productId'=> $particular->getId() ,'price'=> $particular->getProductionPrice(),'unit'=> $unit)));
    }

    private function returnResultData(ProductionItemAmendment $invoice,$msg=''){

        $invoiceItems = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionElement')->particularProductionAmendmentElements($invoice);

        $subTotal = $invoice->getMaterialAmount() > 0 ? $invoice->getMaterialAmount() : 0;
        $quantity = $invoice->getMaterialQuantity() > 0 ? $invoice->getMaterialQuantity() : 0;
        $wasteQuantity = $invoice->getWasteMaterialQuantity() > 0 ? $invoice->getWasteMaterialQuantity() : 0;
        $wasteAmount = $invoice->getWasteAmount() > 0 ? $invoice->getWasteAmount() : 0;
        $data = array(
            'subTotal' => number_format($subTotal,2),
            'quantity' => number_format($quantity,2),
            'wasteQuantity' => number_format($wasteQuantity,2),
            'wasteAmount' => number_format($wasteAmount,2),
            'invoiceItems' => $invoiceItems ,
            'success' => 'success'
        );

        return $data;

    }


    /**
     * @Route("/{id}/create", methods={"POST"}, name="production_build_amendment_create")
     * @param   string
     * @return  bool
     */
    function itemInsertAjax(ProductionItemAmendment $productionItem , Request $reques) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionElement')->insertProductionAmendmentElement($productionItem,$data);
        $result = $this->returnResultData($productionItem);
        return new Response(json_encode($result));

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/update",methods={"GET", "POST"}, name="production_build_amendment_update")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION')")
     */

    public function ajaxSubmit(Request $request, ProductionItemAmendment $entity): Response
    {
        $data = $_REQUEST;

        $em = $this->getDoctrine()->getManager();
        if ($data['wastePercent']) {
            $entity->setWastePercent(floatval($data['wastePercent']));
        }
        if ($data['licenseDate']) {
            $licenseDate = new \DateTime($data['licenseDate']);
            $entity->setLicenseDate($licenseDate);
        }
        if ($data['initiateDate']) {
            $initiateDate = new \DateTime($data['initiateDate']);
            $entity->setInitiateDate($initiateDate);
        }
        if ($data['issueBy']) {
            $entity->setIssueBy($data['issueBy']);
        }
        if ($data['designation']) {
            $entity->setDesignation($data['designation']);
        }
        if ($data['remark']) {
            $entity->setRemark($data['remark']);
        }
        $totalValueAdded = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionValueAdded')->insertAmendmentItemKeyValue($entity,$data);
        if($totalValueAdded > 0){
            $entity->setValueAddedAmount($totalValueAdded);
        }
        $totalElementCost = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionElement')->updateProductionAmendmentElementPrice($entity);
        $em->persist($entity);
        $em->flush();
        return new Response('success');
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="production_amendment_delete")
     * @Security("is_granted('ROLE_DOMAIN')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionItemAmendment::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{entity}/{id}/element-delete", methods={"GET"}, name="production_build_amendment_delete_element")
     * @Security("is_granted('ROLE_DOMAIN')")
     */
    public function elementDelete(ProductionItemAmendment $entity ,$id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionElement::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $totalElementCost = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionElement')->updateProductionAmendmentElementPrice($entity);
        $result = $this->returnResultData($entity);
        return new Response(json_encode($result));
    }


    /**
     * Process a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET"}, name="production_build_amendment_process")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BUILD')")
     */
    public function process($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(ProductionItemAmendment::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        if ($entity->getProcess() == 'created'){
            $entity->setCheckedBy($this->getUser());
            $entity->setProcess('checked');
        }elseif ($entity->getProcess() == 'checked'){
            $entity->setApprovedBy($this->getUser());
            $entity->setProcess('approved');
            $entity->setStatus(true);
            $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionItem')->updateProductionProcess($entity);
        }
        $em->persist($entity);
        $em->flush();
        return new Response('success');
    }


}
