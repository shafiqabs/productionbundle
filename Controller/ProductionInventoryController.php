<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProductionBundle\Controller;

use App\Entity\Application\Inventory;
use App\Entity\Application\Production;
use App\Entity\Core\Setting;
use App\Entity\Core\SettingType;
use App\Form\Core\SettingFormType;
use App\Repository\Core\SettingTypeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\ProductionIssue;
use Terminalbd\ProductionBundle\Entity\ProductionInventory;


/**
 * @Route("/production")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProductionInventoryController extends AbstractController
{

    /**
     * @Route("/inventory", methods={"GET"}, name="production_inventory")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     */
    public function index(Request $request): Response
    {

        return $this->render('@TerminalbdInventory/production/inventory.html.twig');

    }


    /**
     * Search a Setting entity.
     *
     * @Route("/{id}/issue-item-search", methods={"GET"}, name="production_issue_item_search")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_SALES')")
     */

    public function particularSearch(Item $particular)
    {
        $unit = !empty($particular->getUnit() && !empty($particular->getUnit()->getName())) ? $particular->getUnit()->getName():'Unit';
        return new Response(json_encode(array('productId'=> $particular->getId() ,'salesPrice'=> $particular->getSalesPrice(), 'purchasePrice'=> $particular->getPurchasePrice(), 'remaining'=> $particular->getRemainingQuantity(), 'quantity'=> 1 , 'unit'=> $unit)));
    }

    /**
     * Check if mobile available for registering
     * @Route("/issue-item-insert", methods={"POST"}, name="production_create_issue_item")
     * @param   string
     * @return  bool
     */
    function itemInsertAjax(Request $reques) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $item = $this->getDoctrine()->getRepository(Item::class)->find($data['productId']);
        $unit = !empty($item->getUnit() && !empty($item->getUnit()->getName())) ? $item->getUnit()->getName():'';
        $entity = new ProductionIssue();
        $entity->setConfig($config);
        $entity->setQuantity($data['quantity']);
        $entity->setTotalQuantity($data['quantity']);
        $entity->setItem($item);
        $entity->setName($item->getName());
        $entity->setUom($unit);
        $entity->setProcess('created');
        $entity->setPurchasePrice($item->getPurchasePrice());
        $entity->setSubTotal($item->getPurchasePrice() * $entity->getTotalQuantity());
        $em->persist($entity);
        $em->flush();
        return new Response('success');

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="production_issue_delete")
     * @Security("is_granted('ROLE_DOMAIN')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionIssue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }


     /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET"}, name="production_issue_process")
     * @Security("is_granted('ROLE_DOMAIN')")
     */
    public function process($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionIssue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionInventory')->insertProductionInventory($config,$post->getItem());
        $post->setProcess('approved');
        $em->flush();
        return $this->redirectToRoute('production_issue');

    }


    /**
     * @Route("/inventory/data-table", methods={"GET", "POST"}, name="production_inventory_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_SALES')")
     */

    public function inventoryDataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(ProductionInventory::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionInventory')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):

            $processUrl = $this->generateUrl('production_issue_process',array('id'=> $post['id']));
            $records["data"][] = array(
                $id                 = $i,
                $name            = $post['name'],
                $uom        = $post['uom'],
                $quantity             = $post['quantity'],
                $issueQuantity             = $post['issueQuantity'],
                $damageQuantity           = $post['damageQuantity'],
                $returnQuantity            =$post['returnQuantity'],
                $remainigQuantity            =$post['remainigQuantity'],
                $action             ="<div class='btn-group'>
 <a data-action='{$processUrl}' class='btn show green-bg white-font btn-mini' data-id='{$post['id']}' href='javascript:'><i class='feather icon-eye'></i>View</a></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }
}
