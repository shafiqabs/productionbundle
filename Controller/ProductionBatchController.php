<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProductionBundle\Controller;


use App\Entity\Application\Production;
use App\Entity\Core\Vendor;
use App\Repository\Core\SettingTypeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\ProductionBundle\Entity\InvoiceKeyValue;
use Terminalbd\ProductionBundle\Entity\ProductionBatch;
use Terminalbd\ProductionBundle\Entity\ProductionBatchItem;
use Terminalbd\ProductionBundle\Entity\ProductionItem;
use Terminalbd\ProductionBundle\Form\BatchFormContractualType;
use Terminalbd\ProductionBundle\Form\BatchFormType;
use Terminalbd\ProductionBundle\Form\WorkOrderFormType;
use Terminalbd\ProductionBundle\Repository\ItemRepository;

/**
 * @Route("/production/batch")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProductionBatchController extends AbstractController
{

    /**
     * @Route("/", methods={"GET"}, name="production_batch")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function index(Request $request): Response
    {
         return $this->render('@TerminalbdProduction/batch/index.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     * @Route("/{mode}/new", methods={"GET", "POST"}, name="production_batch_create")
     */

    public function create(Request $request,$mode): Response
    {
        $entity = new ProductionBatch();
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $entity->setConfig($config);
        $issueDate = new \DateTime('now');
        $entity->setIssuedate($issueDate);
        $entity->setUpdated($issueDate);
        $entity->setMode($mode);
        $entity->setCreatedBy($this->getUser());
        $entity->setIssuePerson($this->getUser()->getName());
        $entity->setIssueDesignation($this->getUser()->getDesignation()->getName());
        $entity->setProcess('created');
        $em->persist($entity);
        $em->flush();
        return $this->redirectToRoute('production_batch_edit',['id'=> $entity->getId()]);

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="production_batch_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function edit(Request $request, ProductionBatch $entity): Response
    {
        $terminal = $this->getUser()->getTerminal();
        if(in_array($entity->getProcess(),array("checked","approved","closed"))){
            return $this->redirectToRoute('production_batch_show',['id'=> $entity->getId()]);
        }
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $products = $this->getDoctrine()->getRepository("TerminalbdProductionBundle:ProductionItem")->findProducts($config);
        if($entity->getMode() == 'inhouse') {
            $form = $this->createForm(BatchFormType::class, $entity)
                ->add('SaveAndCreate', SubmitType::class);
        }else{
            $terminal = $this->getUser()->getTerminal();
            $form = $this->createForm(BatchFormContractualType::class, $entity ,array('terminal' => $terminal))
                ->add('SaveAndCreate', SubmitType::class);
        }
        $data = $request->request->all();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->upload();
            $this->getDoctrine()->getManager()->flush();
            $this->getDoctrine()->getRepository('TerminalbdProductionBundle:InvoiceKeyValue')->insertBatchKeyValue($entity,$data);
            if($entity->getProcess() == "done"){
                return $this->redirectToRoute('production_batch_show',['id'=> $entity->getId()]);
            }
            return $this->redirectToRoute('production_batch_edit',['id'=> $entity->getId()]);
        }
        if($entity->getMode() == 'inhouse') {
            $mode = "inhouse";
        }else{
            $mode = "contractual";
        }
        return $this->render("@TerminalbdProduction/batch/{$mode}/new.html.twig", [
            'actionUrl' => $this->generateUrl('production_batch_edit',array('id'=> $entity->getId())),
            'dataAction' => $this->generateUrl('production_batch_ajax',array('id'=> $entity->getId())),
            'entity' => $entity,
            'products' => $products,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-submit",methods={"GET", "POST"}, name="production_batch_ajax")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */

    public function ajaxSubmit(Request $request, ProductionBatch $entity): Response
    {
        $data = $_REQUEST;
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $form = $this->createForm(BatchFormType::class, $entity);
        if($entity->getMode() == 'inhouse') {
            $form = $this->createForm(BatchFormType::class, $entity);
        }else{
            $terminal = $this->getUser()->getTerminal();
            $form = $this->createForm(BatchFormContractualType::class, $entity ,array('terminal' => $terminal));
        }
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
        }
        return new Response('success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{meta}/delete-invoice-meta", methods={"GET"}, name="production_batch_meta")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function deleteInvoiceMeta(InvoiceKeyValue $meta): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($meta);
        $em->flush();
        return new Response('success');
    }


    /**
     * Check if mobile available for registering
     * @Route("/{id}/batch-item", methods={"POST"}, name="production_batch_item_create")
     * @param   string
     * @return  bool
     */
    function itemInsertAjax(ProductionBatch $batch , Request $reques) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionBatchItem')->insertAnonymousUpdate($batch,$data);
        return new Response('success');

    }

    /**
     * Check if mobile available for registering
     * @Route("/{id}/batch-item-update", methods={"POST"}, name="production_batch_item_update")
     * @param   string
     * @return  bool
     */
    function itemUpdateAjax(ProductionBatchItem $batch , Request $reques) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionBatchItem')->updateAnonymous($batch,$data);
        return new Response('success');

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/contractual-show",methods={"GET", "POST"}, name="production_batch_item_receive")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */

    public function itemReceive(Request $request, ProductionBatch $entity): Response
    {

        return $this->render('@TerminalbdProduction/batch/contractual/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Check if mobile available for registering
     * @Route("/{id}/batch-item-receive", methods={"POST"}, name="production_batch_item_receive_ajax")
     * @param   string
     * @return  bool
     */
    function itemReceiveAjax(ProductionBatchItem $batch , Request $reques) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionBatchItem')->updateAnonymous($batch,$data);
        return new Response('success');
    }




    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/show",methods={"GET", "POST"}, name="production_batch_show")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */

    public function show(Request $request, ProductionBatch $entity): Response
    {

        if($entity->getMode() == 'inhouse') {
            $mode = "inhouse";
        }else{
            $mode = "contractual";
        }
        return $this->render("@TerminalbdProduction/batch/{$mode}/show.html.twig", [
            'entity' => $entity,
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/{mode}/print",methods={"GET", "POST"}, name="production_batch_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */

    public function print(Request $request, ProductionBatch $entity): Response
    {
        $data = $_REQUEST;
        $em = $this->getDoctrine()->getManager();
        return new Response('success');
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="production_batch_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionBatch::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/item-delete", methods={"GET"}, name="production_batch_item_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function elementDelete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionBatchItem::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }


    /**
     * Process a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET"}, name="production_batch_inhouse_process")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function process($id): Response
    {

        $entity = $this->getDoctrine()->getRepository(ProductionBatch::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        if ($entity->getProcess() == 'done'){
            $entity->setCheckedBy($this->getUser());
            $entity->setProcess('checked');
        }elseif ($entity->getProcess() == 'checked'){
            $entity->setApprovedBy($this->getUser());
            $entity->setProcess('closed');
            $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionExpense')->productionElementExpense($entity);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->productionReceiveInsertQnt($entity);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->productionInhouseExpenseReceiveQnt($entity);
        }
        $em->persist($entity);
        $em->flush();
        return new Response('success');
    }


    /**
     * Process a Setting entity.
     *
     * @Route("/{id}/contractual-process", methods={"GET"}, name="production_batch_contractual_process")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function contractualProcess($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(ProductionBatch::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        if ($entity->getProcess() == 'done') {
            $entity->setCheckedBy($this->getUser());
            $entity->setProcess('checked');
        }elseif($entity->getProcess() == 'checked') {
            $entity->setProcess('approved');
            $entity->setApprovedBy($this->getUser());
            $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionExpense')->productionElementExpense($entity);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->productionInhouseExpenseReceiveQnt($entity);
        }
        $em->flush();
        return new Response('Success');

    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="production_batchdata_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_BATCHSALES')")
     */

    public function inventoryDataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(ProductionBatch::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionBatch')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post ProductionWorkOrder */

        foreach ($result as $post):

            $deleteUrl = $this->generateUrl('production_batch_delete', array('id' => $post['id']));
            $editUrl = $this->generateUrl('production_batch_edit', array('id' => $post['id']));
            $viewUrl = $this->generateUrl('production_batch_show', array('id' => $post['id']));
            $receiveUrl = $this->generateUrl('production_receive_batch_create', array('batch' => $post['id']));
            $checkBox = "";
            if ($post['process'] != "approved") {
                $checkBox = "<input class='process' type='checkbox' name='process' id='process' value='{$post['id']}' /> ";
            }
            $action = '';

            if ($post['process'] == "created" ){
                $action = "<a  href='{$editUrl }' class='btn yellow-bg white-font btn-mini'><i class='feather icon-edit'></i></a>
<a  class='btn green-bg white-font btn-mini'  href='{$viewUrl}' ><i class='feather icon-eye'></i></a>
 <a  data-action='{$deleteUrl}' class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:'><i class='fa fa-remove'></i></a>";
            }elseif ($post['process'] == "checked"){
                $action = "<a   class='btn approve green-bg white-font btn-mini'  href='{$viewUrl}'><i class='feather icon-eye'></i></a>
 <a  data-action='{$deleteUrl}' class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:'><i class='fa fa-remove'></i></a>";
            }else{
                if($post['process'] == "approved" and $post['mode'] == "contractual"){
                    $action = "<a   class='btn green-bg white-font btn-mini'  href='{$viewUrl}' ><i class='feather icon-eye'></i></a><a   class='btn indigo-bg white-font btn-mini'  href='{$receiveUrl}'><i class='feather icon-plus'></i>Receive</a>";
                }else{
                    $action = "<a   class='btn green-bg white-font btn-mini'  href='{$viewUrl}'><i class='feather icon-eye'></i></a>";
                }

            }
            $created = $post['created']->format('d-m-Y');
            $issueDate = $post['issueDate']->format('d-m-Y');

            $records["data"][] = array(
              //  $checkbox           = $checkBox,
                $id                 = $i,
                $created            = $created,
                $issueDate          = $issueDate,
                $invoice            = $post['invoice'],
                $companyName             = $post['companyName'],
                $mode               = ucfirst($post['mode']),
                $process            = ucfirst($post['process']),
                $username           = ucfirst($post['username']),
                $action             ="<div class='btn-group'>{$action}</div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

    /**
     * @Route("/vendor-info", methods={"GET"}, name="production_batch_vendor_info")
     * @param   string
     * @return  bool
     */
    function showVendorAjax(Request $request) : Response
    {
        $post = $_REQUEST['vendor'];
        $vendor = $this->getDoctrine()->getRepository(Vendor::class)->find($post);
        return new Response(json_encode(array('binNo'=> $vendor->getBinNo() ,'vendorMobile'=> $vendor->getMobile(), 'vendorAddress'=> $vendor->getAddress())));

    }

    /**
     * Check if mobile available for registering
     * @Route("/{entity}/create-purchase-vendor", methods={"POST"}, name="production_batch_vendor_create_ajax")
     * @param   string
     * @return  bool
     */
    function createVendorAjax(Request $request,ProductionBatch $entity) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $mode = "creatable";
        $status = $this->getDoctrine()->getRepository(Vendor::class)->checkAvailable($terminal,$mode,$data);
        if($status == 'true') {
            $post = new Vendor();
            $post->setTerminal($terminal);
            $vendorType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['vendorType']));
            if ($vendorType) {
                $post->setVendorType($vendorType);
            }
            $post->setName($data['name']);
            $post->setCompanyName($data['company']);
            $post->setMobile($data['mobile']);
            $businessType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['businessType']));
            if ($businessType) {
                $post->setBusinessType($businessType);
            }
            if($data['address']){
                $post->setAddress($data['address']);
            }
            if($data['binNo']){
                $post->setBinNo($data['binNo']);
            }
            if($data['nidNo']){
                $post->setBinNo($data['nidNo']);
            }
            $this->getDoctrine()->getManager()->persist($post);
            $this->getDoctrine()->getManager()->flush();
            $entity->setVendor($post);
            $this->getDoctrine()->getManager()->persist($entity);
            $this->getDoctrine()->getManager()->flush();
            return new Response('valid');
        }else{
            return new Response('invalid');
        }
    }



}
