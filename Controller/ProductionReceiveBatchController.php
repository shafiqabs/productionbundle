<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProductionBundle\Controller;


use App\Entity\Application\Production;
use App\Entity\Core\Vendor;
use App\Repository\Core\SettingTypeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\ProductionBundle\Entity\InvoiceKeyValue;
use Terminalbd\ProductionBundle\Entity\ProductionBatch;
use Terminalbd\ProductionBundle\Entity\ProductionBatchItem;
use Terminalbd\ProductionBundle\Entity\ProductionItem;
use Terminalbd\ProductionBundle\Entity\ProductionReceiveBatch;
use Terminalbd\ProductionBundle\Entity\ProductionReceiveBatchItem;
use Terminalbd\ProductionBundle\Form\BatchFormContractualType;
use Terminalbd\ProductionBundle\Form\BatchFormType;
use Terminalbd\ProductionBundle\Form\BatchReceiveFormType;
use Terminalbd\ProductionBundle\Form\WorkOrderFormType;
use Terminalbd\ProductionBundle\Repository\ItemRepository;

/**
 * @Route("/production/receive-batch")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProductionReceiveBatchController extends AbstractController
{

    /**
     * @Route("/", methods={"GET"}, name="production_receive_batch")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function index(Request $request): Response
    {
         return $this->render('@TerminalbdProduction/batch/index.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     * @Route("/{batch}/new", methods={"GET", "POST"}, name="production_receive_batch_create")
     */

    public function create(Request $request,ProductionBatch $batch): Response
    {
        $entity = new ProductionReceiveBatch();
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $exist = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionReceiveBatch')->findOneBy(array('batch' => $batch, 'process' => 'created'));
        if(empty($exist)){
            $entity = new ProductionReceiveBatch();
            $issueDate = new \DateTime('now');
            $entity->setConfig($config);
            $entity->setBatch($batch);
            $entity->setIssuedate($issueDate);
            $entity->setUpdated($issueDate);
            $entity->setReceiveDate($issueDate);
            $entity->setCreatedBy($this->getUser());
            $entity->setProcess('created');
            $em->persist($entity);
            $em->flush();
            return $this->redirectToRoute('production_receive_batch_edit',['id'=> $entity->getId()]);
        }else{
            return $this->redirectToRoute('production_receive_batch_edit',['id'=> $exist->getId()]);
        }


    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="production_receive_batch_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function edit(Request $request, ProductionReceiveBatch $entity): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $form = $this->createForm(BatchReceiveFormType::class, $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }
        if(in_array($entity->getProcess(),array("checked","approved"))){
            return $this->redirectToRoute('production_batch');
        }
        return $this->render("@TerminalbdProduction/receivebatch/new.html.twig", [
            'entity' => $entity,
            'actionUrl' => $this->generateUrl('production_receive_batch_edit',array('id'=> $entity->getId())),
            'dataAction' => $this->generateUrl('production_receive_batch_ajax',array('id'=> $entity->getId())),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-submit",methods={"GET", "POST"}, name="production_receive_batch_ajax")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */

    public function ajaxSubmit(Request $request, ProductionReceiveBatch $entity): Response
    {
        $data = $_REQUEST;
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $form = $this->createForm(BatchReceiveFormType::class, $entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
        }
        return new Response('success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{meta}/delete-invoice-meta", methods={"GET"}, name="production_receive_batch_meta")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function deleteInvoiceMeta(InvoiceKeyValue $meta): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($meta);
        $em->flush();
        return new Response('success');
    }


    /**
     * Check if mobile available for registering
     * @Route("/{id}/batch-item", methods={"POST"}, name="production_receive_batch_item_create")
     * @param   string
     * @return  bool
     */
    function itemInsertAjax(ProductionBatch $batch , Request $reques) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionBatchItem')->insertAnonymousUpdate($batch,$data);
        return new Response('success');

    }

    /**
     * Check if mobile available for registering
     * @Route("/{id}/batch-item-update", methods={"POST"}, name="production_receive_batch_item_update")
     * @param   string
     * @return  bool
     */
    function itemUpdateAjax(ProductionBatchItem $batch , Request $reques) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionBatchItem')->updateAnonymous($batch,$data);
        return new Response('success');

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/contractual-show",methods={"GET", "POST"}, name="production_receive_batch_item_receive")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */

    public function itemReceive(Request $request, ProductionBatch $entity): Response
    {

        return $this->render('@TerminalbdProduction/batch/contractual/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Check if mobile available for registering
     * @Route("/{batch}/{item}/batch-item-receive", methods={"POST"}, name="production_receive_batch_item_receive_ajax")
     * @param   string
     * @return  bool
     */
    function itemReceiveAjax(ProductionReceiveBatch $batch , ProductionBatchItem $item , Request $reques) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionReceiveBatchItem')->contractualReceive($batch,$item,$data);
        return new Response('success');
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/show",methods={"GET", "POST"}, name="production_receive_batch_show")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */

    public function show(Request $request, ProductionBatch $entity): Response
    {

        return $this->render('@TerminalbdProduction/batch/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/{mode}/print",methods={"GET", "POST"}, name="production_receive_batch_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */

    public function print(Request $request, ProductionBatch $entity): Response
    {
        $data = $_REQUEST;
        $em = $this->getDoctrine()->getManager();
        return new Response('success');
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="production_receive_batch_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionBatch::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/item-delete", methods={"GET"}, name="production_receive_batch_item_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function elementDelete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionReceiveBatchItem::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }



    /**
     * Process a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET"}, name="production_receive_batch_process")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION_BATCH')")
     */
    public function process($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionReceiveBatch::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        if($post->getChallanNo()){
            $post->setProcess('approved');
            $post->setCheckedBy($this->getUser());
            $post->setApprovedBy($this->getUser());
            if($post->getProcess() == 'approved'){
                $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionReceiveBatchItem')->productionReceiveApprove($post);
                $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->contractualProductionStock($post);
                $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->contractualProductionStock($post);
            }
            $em->flush();
        }
        return new Response('Success');

    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="production_receive_batchdata_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_BATCHSALES')")
     */

    public function inventoryDataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(ProductionBatch::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionBatch')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post ProductionWorkOrder */

        foreach ($result as $post):

            $deleteUrl = $this->generateUrl('production_batch_delete', array('id' => $post['id']));
            $editUrl = $this->generateUrl('production_batch_edit', array('id' => $post['id']));
            $viewUrl = $this->generateUrl('production_batch_show', array('id' => $post['id']));
            $processUrl = $this->generateUrl('production_batch_process', array('id' => $post['id']));
            $receiveUrl = $this->generateUrl('production_batch_item_receive', array('id' => $post['id']));
            $checkBox = "";
            if ($post['process'] != "approved") {
                $checkBox = "<input class='process' type='checkbox' name='process' id='process' value='{$post['id']}' /> ";
            }
            $action = '';

            if ($post['process'] == "created" ){
                $action = "<a  href='{$editUrl }' class='btn yellow-bg white-font btn-mini'><i class='feather icon-edit'></i></a>
<a  class='btn green-bg white-font btn-mini'  href='{$viewUrl}' ><i class='feather icon-eye'></i></a>
 <a  data-action='{$deleteUrl}' class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:'><i class='fa fa-remove'></i></a>";
            }elseif ($post['process'] == "checked"){
                $action = "<a   class='btn approve green-bg white-font btn-mini'  href='{$viewUrl}'><i class='feather icon-eye'></i></a>
<a  data-action='{$processUrl}' class='btn approve green-bg white-font btn-mini' data-id='{$post['id']}' href='javascript:'><i class='feather icon-check-square'></i>Approve</a>
 <a  data-action='{$deleteUrl}' class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:'><i class='fa fa-remove'></i></a>";
            }else{
                $action = "<a   class='btn green-bg white-font btn-mini'  href='{$viewUrl}' id='postView-{$post['id']}'><i class='feather icon-eye'></i></a><a   class='btn indigo-bg white-font btn-mini'  href='{$receiveUrl}'><i class='feather icon-plus'></i>Receive</a>";
            }
            $created = $post['created']->format('d-m-Y');
            $issueDate = $post['issueDate']->format('d-m-Y');

            $records["data"][] = array(
              //  $checkbox           = $checkBox,
                $id                 = $i,
                $created            = $created,
                $issueDate          = $issueDate,
                $invoice            = $post['invoice'],
                $companyName             = $post['companyName'],
                $mode               = ucfirst($post['mode']),
                $process            = ucfirst($post['process']),
                $username           = ucfirst($post['username']),
                $action             ="<div class='btn-group'>{$action}</div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

    /**
     * @Route("/vendor-info", methods={"GET"}, name="production_receive_batch_vendor_info")
     * @param   string
     * @return  bool
     */
    function showVendorAjax(Request $request) : Response
    {
        $post = $_REQUEST['vendor'];
        $vendor = $this->getDoctrine()->getRepository(Vendor::class)->find($post);
        return new Response(json_encode(array('binNo'=> $vendor->getBinNo() ,'vendorMobile'=> $vendor->getMobile(), 'vendorAddress'=> $vendor->getAddress())));

    }

    /**
     * Check if mobile available for registering
     * @Route("/{entity}/create-purchase-vendor", methods={"POST"}, name="production_receive_batch_vendor_create_ajax")
     * @param   string
     * @return  bool
     */
    function createVendorAjax(Request $request,ProductionBatch $entity) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $mode = "creatable";
        $status = $this->getDoctrine()->getRepository(Vendor::class)->checkAvailable($terminal,$mode,$data);
        if($status == 'true') {
            $post = new Vendor();
            $post->setTerminal($terminal);
            $vendorType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['vendorType']));
            if ($vendorType) {
                $post->setVendorType($vendorType);
            }
            $post->setName($data['name']);
            $post->setCompanyName($data['company']);
            $post->setMobile($data['mobile']);
            $businessType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['businessType']));
            if ($businessType) {
                $post->setBusinessType($businessType);
            }
            if($data['address']){
                $post->setAddress($data['address']);
            }
            if($data['binNo']){
                $post->setBinNo($data['binNo']);
            }
            if($data['nidNo']){
                $post->setBinNo($data['nidNo']);
            }
            $this->getDoctrine()->getManager()->persist($post);
            $this->getDoctrine()->getManager()->flush();
            $entity->setVendor($post);
            $this->getDoctrine()->getManager()->persist($entity);
            $this->getDoctrine()->getManager()->flush();
            return new Response('valid');
        }else{
            return new Response('invalid');
        }
    }



}
