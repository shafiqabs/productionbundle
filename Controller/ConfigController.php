<?php

namespace Terminalbd\ProductionBundle\Controller;
use App\Entity\Application\Inventory;
use App\Entity\Application\Production;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Form\ConfigFormType;

/**
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
 * @Route("/production/config")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ConfigController extends AbstractController
{


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/", methods={"GET", "POST"}, name="production_config")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_inventory_setting')")
     */
    public function edit(Request $request): Response
    {
        $entity = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $data = $request->request->all();
        $form = $this->createForm(\Terminalbd\ProductionBundle\Form\ConfigFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'post.updated_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('production_config');
            }

            return $this->redirectToRoute('production_config');
        }
        return $this->render('@TerminalbdProduction/config/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


}
