<?php
namespace Terminalbd\ProductionBundle\Form;

use App\Entity\Application\Accounting;
use App\Entity\Application\Nbrvat;
use App\Entity\Application\Production;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ConfigFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('productionProcedure', ChoiceType::class, [
                'choices'  => ['Direct-Stock' => 'direct-stock','Production-Stock' => 'Production-Stock'],
                'required'    => false,
                'placeholder' => 'Choose a Process',
                'attr' => ['autofocus' => true,'class'=>'process'],
            ])
            ->add('consumptionMethod', ChoiceType::class, [
                'choices'  => ['Direct-Consumption' => 'direct-consumption','Purachse Batch wise Consumption' => 'purachse-consumption'],
                'required'    => false,
                'placeholder' => 'Choose a Process',
                'attr' => ['autofocus' => true,'class'=>'process'],
            ])
        ;

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Production::class,
        ]);
    }
}
