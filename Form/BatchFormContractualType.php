<?php

namespace Terminalbd\ProductionBundle\Form;

use App\Entity\Admin\Terminal;
use App\Entity\Core\Vendor;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProductionBundle\Entity\ProductionBatch;
use Terminalbd\ProductionBundle\Entity\ProductionWorkOrder;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class BatchFormContractualType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $terminal =  $options['terminal']->getId();
        $builder
            ->add('remark', TextareaType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'action textarea text-left',
                    'placeholder'=>'Enter remark',
                ],
                'required' => false
            ])
             ->add('address', TextareaType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'action textarea text-left',
                    'placeholder'=>'Enter destination address',
                ],
                'required' => false
            ])
            ->add('issuePerson', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'action text-left',
                    'placeholder'=>'Enter issue person',
                ],
                'required' => true
            ])
            ->add('issueDesignation', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'action text-left',
                    'placeholder'=>'Enter issue designation',
                ],
                'required' => true
            ])
            ->add('vendor', EntityType::class, array(
                'required'    => true,
                'class' => Vendor::class,
                'placeholder' => 'Choose a vendor',
                'choice_label' => 'companyName',
                'attr'=>array('class'=>'form-control vendor select2 action'),
                'query_builder' => function(EntityRepository $er) use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('process', ChoiceType::class, [
                'choices'  => ['Created' => 'created','Done' => 'done'],
                'required'    => false,
                'placeholder' => 'Choose a Process',
                'attr' => ['autofocus' => true,'class'=>'process action'],
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductionBatch::class,
            'terminal' => Terminal::class,
        ]);
    }



}
