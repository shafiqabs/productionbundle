<?php

namespace Terminalbd\ProductionBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProductionBundle\Entity\ProductionWorkOrder;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class WorkOrderFormType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('remark', TextareaType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'action textarea text-left',
                    'placeholder'=>'Enter remark',
                ],
                'required' => false
            ])
            ->add('requsitionNo', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Enter requsition no','class'=>'amount action'],
                'required' => false
            ])
            ->add('issueDate', DateType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'action date-picker',
                ],
                'widget' => 'single_text',
                'html5' => false,
            ])
            ->add('receiveDate', DateType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'action date-picker',
                ],
                'widget' => 'single_text',
                'html5' => false,
            ])
            ->add('file', null, [
                'required' => false,
                'attr' => [
                    'class'=>'form-file-input action'
                ]
            ])
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductionWorkOrder::class,
        ]);
    }



}
