<?php

namespace Terminalbd\ProductionBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProductionBundle\Entity\ProductionBatch;
use Terminalbd\ProductionBundle\Entity\ProductionReceiveBatch;
use Terminalbd\ProductionBundle\Entity\ProductionWorkOrder;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class BatchReceiveFormType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('challanNo', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'action text-left',
                    'placeholder'=>'Enter challan no',
                ],
                'required' => true
            ])
            ->add('receiveDate', DateType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'action date-picker',
                ],
                'widget' => 'single_text',
                'html5' => false,
                'required' => true
            ])

            /*->add('receiveTime', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'timePicker'
                ],
                'required' => true
            ])*/;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductionReceiveBatch::class,
        ]);
    }



}
