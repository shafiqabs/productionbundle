<?php

namespace Terminalbd\ProductionBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Terminalbd\ProductionBundle\Entity\ProductionReceiveBatch;

class ReceiveBatchListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->createCode($args);
    }

    public function createCode(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "ProductionBatch" entity

        if ($entity instanceof ProductionReceiveBatch) {

            $datetime = new \DateTime("now");
            $lastCode = $this->getLastCode($args, $datetime, $entity);
            $code = $lastCode + 1;
            $entity->setCode($code);
            $entity->setInvoice(sprintf("%s%s%s","PRB-",$datetime->format('my'), str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));

        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @param $datetime
     * @param $entity
     * @return int|mixed
     */
    public function getLastCode(LifecycleEventArgs $args, $datetime, ProductionReceiveBatch $entity)
    {
        $start = $datetime->format('Y-m-01 00:00:00');
        $end = $datetime->format('Y-m-t 23:59:59');

        $entityManager = $args->getEntityManager();
        $qb = $entityManager->getRepository('TerminalbdProductionBundle:ProductionReceiveBatch')->createQueryBuilder('s');

        $qb
            ->select('MAX(s.code)')
            ->where('s.config = :config')->setParameter('config', $entity->getConfig())
            ->andWhere('s.created >= :start')->setParameter('start', $start)
            ->andWhere('s.created <= :end')->setParameter('end', $end);
        $lastCode = $qb->getQuery()->getSingleScalarResult();
        if (empty($lastCode)) {
            return 0;
        }
        return intval($lastCode);
    }
}